using UnityEngine;
using System.Collections.Generic;
using System;
using Tanks.Weapons;
using Tanks.Utility;

namespace Tanks.Tank
{
    public class TankTurret : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed = 5.0f;
        [SerializeField] private Transform projectileFirePoint;
        private GameObjectPool<Projectile> projectilePool;

        private LinkedList<GameObject> activeProjectiles;
        private Transform turretTransform;
        private Vector3 lookAt;

        private void Awake()
        {
            turretTransform = transform;
            activeProjectiles = new LinkedList<GameObject>();
        }

        private void Update()
        {
            Quaternion currentRotation = turretTransform.rotation;
            Quaternion lookAtRotation = Quaternion.LookRotation((lookAt - turretTransform.position).normalized, turretTransform.up);
            turretTransform.rotation = Quaternion.Lerp(currentRotation, lookAtRotation, Time.deltaTime * rotationSpeed);
        }

        public void SetProjectilePrefab(GameObject projectilePrefab)
        {
            projectilePool = new GameObjectPool<Projectile>(projectilePrefab, 5);
        }

        public void LookAt(Vector3 position)
        {
            position.y = turretTransform.position.y;
            lookAt = position;
        }

        public int ActiveProjectileCount()
        {
            int activeCount = 0;
            for(LinkedListNode<GameObject> node = activeProjectiles.First; node != null;)
            {
                if (node.Value.activeSelf)
                {
                    activeCount++;
                    node = node.Next;
                }
                else
                {
                    LinkedListNode<GameObject> toRemove = node;
                    node = node.Next;
                    activeProjectiles.Remove(toRemove);
                }
            }

            return activeCount;
        }

        public void Shoot()
        {
            if(projectilePool != null)
            {
                Tuple<GameObject, Projectile> projectileObject = projectilePool.GetPair(projectileFirePoint.position, Quaternion.LookRotation(projectileFirePoint.forward, projectileFirePoint.up));
                activeProjectiles.AddLast(projectileObject.Item1);
                projectileObject.Item2.Shoot(projectileFirePoint.forward);
            }
        }

        public Transform GetShootPoint()
        {
            return projectileFirePoint;
        }


        /// <summary>
        /// Returns the normalized aim direction
        /// </summary>
        public Vector3 GetAimDirection()
        {
            return projectileFirePoint.forward;
        }
    }
}

