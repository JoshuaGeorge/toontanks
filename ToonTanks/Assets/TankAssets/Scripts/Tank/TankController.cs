using UnityEngine;
using Tanks.Game;
using Tanks.Interfaces;

namespace Tanks.Tank
{

    public class TankController : MonoBehaviour, IDrawDebugDisplay
    {
        [SerializeField] protected TankMovement movementComponent;
        [SerializeField] protected TankTurret turretComponent;
        [SerializeField] protected Health healthComponent;
        [SerializeField] protected GameObject destroyedMarker;

        private int maximumAliveProjectilesAllowed = 5;
        private float lastProjectileFiredTime = 0.0f;
        private float projectileFireRate;
        private bool isAllowedToShoot = false;


        // Start is called before the first frame update
        protected virtual void Start()
        {
            healthComponent.OnDieEvent.AddListener(OnTankDestroyed);
            healthComponent.OnRespawnEvent.AddListener(OnTankRespawn);
            lastProjectileFiredTime = Time.time;
        }

        public virtual void Equip(IEquipment equipment)
        {
        }

        public virtual void AssignLevelDetails(LevelDetails levelDetails)
        {
        }

        public virtual void SetControl(bool enable)
        {
            movementComponent.SetCanMove(enable);
            isAllowedToShoot = enable;
        }

        public void SetMovementSpeed(float speed)
        {
            movementComponent.SetSpeed(speed);
        }

        public void SetProjectileFireRate(float firerate)
        {
            this.projectileFireRate = firerate;
        }

        public void SetMaximumAliveProjectilesLimit(int limit)
        {
            maximumAliveProjectilesAllowed = limit;
        }

        public void SetProjectile(GameObject projectilePrefab)
        {
            turretComponent.SetProjectilePrefab(projectilePrefab);
        }

        public bool CanShootProjectile()
        {
            return HasProjectilesLeft() && HasProjectileCooldownEnded() && isAllowedToShoot;
        }

        public Vector3 GetPosition()
        {
            return movementComponent.GetWorldSpacePos();
        }

        public Vector2 GetPosition2D()
        {
            return new Vector2(movementComponent.GetWorldSpacePos().x, movementComponent.GetWorldSpacePos().z);
        }

        public Vector3 GetVelocity()
        {
            return movementComponent.GetVelocity();
        }

        public Vector2 GetVelocity2D()
        {
            Vector3 velocity = movementComponent.GetVelocity();
            return new Vector2(velocity.x, velocity.z);
        }

        public Vector3 GetTurretAimDirection()
        {
            return turretComponent.GetAimDirection();
        }

        public Transform GetTurretTransform()
        {
            return turretComponent.GetShootPoint();
        } 

        public float GetMovementSpeed()
        {
            return movementComponent.GetSpeed();
        }

        public bool IsDead()
        {
            return healthComponent.IsDead();
        }

        public bool IsAllowedToShoot()
        {
            return isAllowedToShoot;
        }

        protected bool HasProjectilesLeft()
        {
            return turretComponent.ActiveProjectileCount() < maximumAliveProjectilesAllowed;
        }

        protected bool HasProjectileCooldownEnded()
        {
            return (Time.time - lastProjectileFiredTime) > projectileFireRate;
        }

        public virtual void Shoot()
        {
            if (CanShootProjectile())
            {
                turretComponent.Shoot();
                lastProjectileFiredTime = Time.time;
            }
        }

        protected virtual void OnTankDestroyed()
        {
            destroyedMarker.SetActive(true);
        }

        protected virtual void OnTankRespawn()
        {
            destroyedMarker.SetActive(false);
        }

        public virtual void OnDrawDebugDisplay(CanvasDebugDisplay debugDisplay)
        {
        }
    }
}


