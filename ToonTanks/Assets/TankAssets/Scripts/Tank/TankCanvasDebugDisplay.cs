using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tanks.Tank;

public class TankCanvasDebugDisplay : MonoBehaviour
{
    [SerializeField]
    private CanvasDebugDisplay canvasDebugDisplayPrefab;

    [SerializeField]
    private float heightPositionOffset = 10.0f;

    private TankController controller;
    private CanvasDebugDisplay canvasDebugDisplayInstance;

    private void Awake()
    {
        controller = GetComponent<TankController>();

        if (controller)
        {
            canvasDebugDisplayInstance = Instantiate(canvasDebugDisplayPrefab);
        }
    }

    private void LateUpdate()
    {
        canvasDebugDisplayInstance.transform.position = transform.position + new Vector3(0.0f, heightPositionOffset, 0.0f);
        controller.OnDrawDebugDisplay(canvasDebugDisplayInstance);
    }
}
