using UnityEngine.Events;
using UnityEngine;
using Tanks.Interfaces;
using Tanks.Structs;

namespace Tanks.Tank
{
    public class Health : MonoBehaviour, IDamageable
    {
        [SerializeField] private int startingHealth = 100;
        [SerializeField] private int maxHealth = 100;
        [SerializeField] private UnityEvent onDie;
        [SerializeField] private UnityEvent onRespawn;

        private int currentHealth = 100;

        public UnityEvent OnDieEvent => onDie;
        public UnityEvent OnRespawnEvent => onRespawn;

        private void Start()
        {
            currentHealth = startingHealth;
        }

        public void TakeDamage(Damage damage)
        {
            if (IsDead())
            {
                return;
            }

            currentHealth -= Mathf.Min(currentHealth, damage.Value);

            if (IsDead())
            {
                onDie.Invoke();
            }
        }

        public void Respawn(bool withMaxHealth = false)
        {
            currentHealth = (withMaxHealth) ? maxHealth : startingHealth;
            onRespawn.Invoke();
        }

        public bool IsDead()
        {
            return currentHealth == 0;
        }
    }
}

