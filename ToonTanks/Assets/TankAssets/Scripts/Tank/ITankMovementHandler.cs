using UnityEngine;

public interface ITankMovementHandler
{
    void Move(Vector2 inputAxis, float deltaTime);
    void Turn(Vector2 inputAxis, float deltaTime);
    void SetMoveSpeed(float speed);
}
