using UnityEngine;

namespace Tanks.Tank
{
    public class TankMovement : MonoBehaviour
    {
        [SerializeField] private float speed = 12.0f;
        [SerializeField] private float turnSpeed = 180.0f;
        [SerializeField] private bool useManhattanMovement = true;

        private Rigidbody rigidBody;
        private ITankMovementHandler movementHandler;

        private Vector2 inputAxis;
        private bool canMove = true;
        private Vector3 previousFramePosition;
        private Vector3 velocity;
       
        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            movementHandler = new DirectionBasedMovement(rigidBody, transform, speed, turnSpeed);
            previousFramePosition = new Vector3(0.0f, 0.0f, 0.0f);
            velocity = new Vector3(0.0f, 0.0f, 0.0f);
        }

        private void OnEnable()
        {
            // When the tank is turned on, make sure it's not kinematic.
            rigidBody.isKinematic = false;

            // Also reset the input values.
            inputAxis = new Vector2(0.0f, 0.0f);
        }

        private void OnDisable()
        {
            // When the tank is turned off, set it to kinematic so it stops moving.
            rigidBody.isKinematic = true;
        }

        private void FixedUpdate()
        {
            if (!canMove) return;

            float dt = Time.deltaTime;

            if (useManhattanMovement && inputAxis != Vector2.zero)
                inputAxis = VectorToManhattan(inputAxis.normalized);

            // Adjust the rigidbodies position and orientation in FixedUpdate.
            movementHandler.Turn(inputAxis, dt);
            movementHandler.Move(inputAxis, dt);

            velocity = (rigidBody.position - previousFramePosition) / dt;
            previousFramePosition = rigidBody.position;
        }

        public void Move(Vector2 inputAxis)
        {
            this.inputAxis = inputAxis;
        }

        public Vector3 GetForwardVector()
        {
            return transform.forward;
        }

        public Vector3 GetVelocity()
        {
            return velocity;
            //return GetForwardVector() * speed;
        }

        public Vector3 GetWorldSpacePos()
        {
            return transform.position;
        }

        public void SetCanMove(bool canMove)
        {
            this.canMove = canMove;
        }

        public void SetSpeed(float speed)
        {
            this.speed = speed;
            movementHandler.SetMoveSpeed(speed);
        }

        public float GetSpeed()
        {
            return speed;
        }

        private Vector2 VectorToManhattan(Vector2 input)
        {
            float angle = Mathf.Atan2(input.y, input.x) * Mathf.Rad2Deg;

            if(angle < 0)
            {
                angle = 360.0f + angle;
            }

            float[] angles = new float[] { 0.0f, 45.0f, 90.0f, 135.0f, 180.0f, 225.0f, 270.0f, 315.0f, 360.0f };
            float smallestDelta = float.MaxValue;
            float closestAngle = 0.0f;
            for (int i = 0; i < angles.Length; i++)
            {
                if( Mathf.Abs(angles[i] - angle) < smallestDelta)
                {
                    smallestDelta = Mathf.Abs(angles[i] - angle);
                    closestAngle = angles[i];
                }
            }

            closestAngle = closestAngle * Mathf.Deg2Rad;
            return new Vector2(Mathf.Cos(closestAngle), Mathf.Sin(closestAngle));
        }
    }

    ///////////////////////////////////////////////////
    /// -- Direction-based Movement handler
    ///////////////////////////////////////////////////
    ///

    class DirectionBasedMovement : ITankMovementHandler
    {
        private Rigidbody rb;
        private Transform transform;
        private float moveSpeed;
        private float turnSpeed;

        public DirectionBasedMovement(Rigidbody rigidbody, Transform transform, float moveSpeed, float turnSpeed)
        {
            rb = rigidbody;
            this.transform = transform;
            this.moveSpeed = moveSpeed;
            this.turnSpeed = turnSpeed;
        }

        public void Move(Vector2 inputAxis, float deltaTime)
        {
            Vector3 desiredMoveDirection = new Vector3(inputAxis.x, 0.0f, inputAxis.y).normalized;
            Vector3 movement = desiredMoveDirection * Mathf.Abs(Vector2.Dot(new Vector2(desiredMoveDirection.x, desiredMoveDirection.z), new Vector2(transform.forward.x, transform.forward.z))) * moveSpeed * deltaTime;

            // Apply this movement to the rigidbody's position.
            rb.MovePosition(rb.position + movement);
        }

        public void SetMoveSpeed(float speed)
        {
            moveSpeed = speed;
        }

        public void Turn(Vector2 inputAxis, float deltaTime)
        {
            if (inputAxis.x == 0 && inputAxis.y == 0) return;

            Vector2 desiredMoveDirection = inputAxis.normalized;
            Vector2 tankForwardDirection = new Vector2(transform.forward.x, transform.forward.z).normalized;

            float targetRotation = Mathf.Acos(Mathf.Clamp(Vector2.Dot(tankForwardDirection, desiredMoveDirection), -1.0f, 1.0f)) * Mathf.Rad2Deg;

            bool turnRight = Vector2.Dot(new Vector2(transform.right.x, transform.right.z), desiredMoveDirection) >= 0.0f;

            if (targetRotation > 90.0f)
            {
                targetRotation = Mathf.Acos(Mathf.Clamp(Vector2.Dot(-tankForwardDirection, desiredMoveDirection), -1.0f, 1.0f)) * Mathf.Rad2Deg;
                turnRight = !turnRight;
            }

            float angle = targetRotation * turnSpeed * Time.deltaTime;

            Quaternion toRotation = Quaternion.Euler(0.0f, angle * (turnRight ? 1.0f : -1.0f), 0.0f);

            // Apply this rotation to the rigidbody's rotation.
            rb.MoveRotation(transform.rotation * toRotation);
        }
    }

    ///////////////////////////////////////////////////
    /// -- Rotation-based Movement handler
    ///////////////////////////////////////////////////
    ///

    class RotationBasedMovement : ITankMovementHandler
    {
        private Rigidbody rb;
        private Transform transform;
        private float moveSpeed;
        private float turnSpeed;

        public RotationBasedMovement(Rigidbody rigidbody, Transform transform, float moveSpeed, float turnSpeed)
        {
            rb = rigidbody;
            this.transform = transform;
            this.moveSpeed = moveSpeed;
            this.turnSpeed = turnSpeed;
        }

        public void Move(Vector2 inputAxis, float deltaTime)
        {
            // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
            Vector3 movement = transform.forward * inputAxis.y * moveSpeed * deltaTime;

            // Apply this movement to the rigidbody's position.
            rb.MovePosition(rb.position + movement);
        }

        public void SetMoveSpeed(float speed)
        {
            moveSpeed = speed;
        }

        public void Turn(Vector2 inputAxis, float deltaTime)
        {
            // Determine the number of degrees to be turned based on the input, speed and time between frames.
            float turn = inputAxis.x * turnSpeed * deltaTime;

            // Make this into a rotation in the y axis.
            Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

            // Apply this rotation to the rigidbody's rotation.
            rb.MoveRotation(rb.rotation * turnRotation);
        }
    }
}







