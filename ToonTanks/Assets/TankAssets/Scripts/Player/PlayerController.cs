using UnityEngine;
using UnityEngine.InputSystem;
using Tanks.Tank;
using Tanks.Weapons;
using Tanks.Interfaces;

namespace Tanks.Player
{
    public class PlayerController : TankController
    {
        [SerializeField] private GameObject playerHUDCanvasPrefab;

        [SerializeField] private LineRenderer aimLineRenderer;
        [SerializeField] private TrailRenderer cursorTrailRenderer;

        [SerializeField] private BaseEquipment startingEquipmentPrefebRef;

        private AimCursor turretCursor;
        private EquipmentSlot equipmentSlot;

        private void Awake()
        {
            turretCursor = Instantiate(playerHUDCanvasPrefab).GetComponent<AimCursor>();
            equipmentSlot = new EquipmentSlot();
            if (startingEquipmentPrefebRef)
            {
                BaseEquipment startingEquipment = Instantiate(startingEquipmentPrefebRef);
                Equip(startingEquipment);
            }
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            Vector2 inputAxis = context.ReadValue<Vector2>();
            movementComponent.Move(inputAxis);
        }

        public void OnAimMouse(InputAction.CallbackContext context)
        {
            Vector2 pixelSpaceMousePosition = context.ReadValue<Vector2>();
            turretCursor.SetPosition(pixelSpaceMousePosition);
        }

        public void OnAimGamepad(InputAction.CallbackContext context)
        {
            Vector2 deltaMovement = context.ReadValue<Vector2>();
            turretCursor.MoveCursor(deltaMovement);
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Shoot();
            }
        }

        public void UseEquipment(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                equipmentSlot.TryUseEquipment();
            }
        }

        public void AssignTank(Tank.TankMovement movement, Tank.TankTurret turret, Tank.Health health)
        {
            movementComponent = movement;
            turretComponent = turret;
            healthComponent = health;
        }

        public override void SetControl(bool enable)
        {
            base.SetControl(enable);
            aimLineRenderer.forceRenderingOff = !enable;
        }

        private void Update()
        {
            if (healthComponent.IsDead()) return;

            AimTurret();

            cursorTrailRenderer.transform.position = turretCursor.GetWorldSpaceCursorPosition() + new Vector3(0.0f, 0.5f, 0.0f);
        }

        private void AimTurret()
        {
            Vector3 worldSpaceCursorPosition = turretCursor.GetWorldSpaceCursorPosition();
            Vector3 localSpaceCursorPosition = transform.InverseTransformPoint(worldSpaceCursorPosition);
            // update the line renderer
            aimLineRenderer.SetPosition(0, transform.InverseTransformPoint(turretComponent.GetShootPoint().position));
            aimLineRenderer.SetPosition(1, localSpaceCursorPosition + new Vector3(0.0f, 0.5f, 0.0f));

            // update the cursor in the HUD
            turretComponent.LookAt(worldSpaceCursorPosition);
        }

        public override void Equip(IEquipment equipment)
        {
            equipment.OnEquipped(transform);
            equipmentSlot.SetEquipment(equipment);
        }

        protected override void OnTankDestroyed()
        {
            base.OnTankDestroyed();

            SetControl(false);
        }

#if DEVELOPMENT || UNITY_EDITOR
        public override void OnDrawDebugDisplay(CanvasDebugDisplay debugDisplay)
        {
            debugDisplay.SetDebugText("RB Vel", GetVelocity2D().ToString());
        }
#endif
    }
}

