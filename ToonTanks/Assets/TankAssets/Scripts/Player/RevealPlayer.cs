using System.Collections.Generic;
using UnityEngine;

namespace Tanks.Player
{
    [RequireComponent(typeof(Camera))]
    public class RevealPlayer : MonoBehaviour
    {
        [SerializeField] private List<Transform> transformsToReveal = new List<Transform>();
        [SerializeField] private LayerMask revealableLayers;

        private Camera cam;
        private RaycastHit[] hitResults;
        private int CUTOUT_POS_PROPERTY = Shader.PropertyToID("_CutoutPos");

        private void Start()
        {
            cam = GetComponent<Camera>();
            hitResults = new RaycastHit[5];
        }

        private void Update()
        {
            for (int i = 0; i < transformsToReveal.Count; i++)
            {
                Transform target = transformsToReveal[i];

                Vector2 targetScreenSpacePos = cam.WorldToScreenPoint(target.position) / new Vector2(Screen.width, Screen.height);

                Ray ray = new Ray(transform.position, target.position - transform.position);

                int hitCount = Physics.SphereCastNonAlloc(ray, 1.0f, hitResults, 50.0f, revealableLayers.value);

                for (int j = 0; j < hitCount; j++)
                {
                    MeshRenderer renderer = hitResults[j].collider.gameObject.GetComponent<MeshRenderer>();
                    if (renderer)
                    {
                        for (int k = 0; k < renderer.sharedMaterials.Length; k++)
                        {
                            renderer.sharedMaterials[k].SetVector(CUTOUT_POS_PROPERTY, targetScreenSpacePos);
                        }
                    }
                }
            }
        }

        public void AddPlayer(GameObject player)
        {
            transformsToReveal.Add(player.transform);
        }
    }
}

