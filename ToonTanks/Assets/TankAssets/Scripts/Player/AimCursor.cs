using UnityEngine;

namespace Tanks.Player
{
    public class AimCursor : MonoBehaviour
    {
        [SerializeField] private RectTransform cursorTransform;
        [SerializeField] private float cursorSpeed = 10.0f;

        private Vector2 cursorScreenPosition;
        private LayerMask groundMask;
        private Vector3 lastValidWorldSpaceCursorPosition;

        bool dirty = true;

        private void Start()
        {
            groundMask = LayerMask.GetMask("Ground");
        }

        private void Update()
        {
            if (dirty)
            {
                cursorTransform.position = cursorScreenPosition;
                dirty = false;
            }
        }

        public void MoveCursor(Vector2 move)
        {
            if(move.x != 0 && move.y != 0)
            {
                cursorScreenPosition += move * cursorSpeed;
                dirty = true;
            }
        }

        public void SetPosition(Vector2 absolutePosition)
        {
            if(absolutePosition != cursorScreenPosition)
            {
                cursorScreenPosition = absolutePosition;
                dirty = true;
            }
        }

        public Vector3 GetWorldSpaceCursorPosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(cursorScreenPosition);       // TODO: Do not use camera.main.
            Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 9999.0f, groundMask.value))
            {
                lastValidWorldSpaceCursorPosition = hit.point;
            }
            
            return lastValidWorldSpaceCursorPosition;
        }
    }
}

