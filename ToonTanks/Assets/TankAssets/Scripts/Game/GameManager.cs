using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tanks.Game
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private GameObject LevelSelector;

        private Scene levelSelectorScene;
        private Scene currentlyLoadedLevel;
        private int currentlyLoadedLevelNumber = 0;

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            levelSelectorScene = SceneManager.GetActiveScene();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public int GetLevelNumber()
        {
            return currentlyLoadedLevelNumber;
        }

        public int GetPlayerLives()
        {
            return 3;
        }

        public void LoadLevel(string sceneName, int levelNumber)
        {
            currentlyLoadedLevelNumber = levelNumber;
            LevelSelector.SetActive(false);
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        public void ReturnToLevelSelector()
        {
            SceneManager.SetActiveScene(levelSelectorScene);
            LevelSelector.SetActive(true);
            SceneManager.UnloadSceneAsync(currentlyLoadedLevel);
        }

        public void LoadNextLevel()
        {
            if(currentlyLoadedLevel.buildIndex == SceneManager.sceneCountInBuildSettings - 1)
            {
                ReturnToLevelSelector();
            }
            else
            {
                currentlyLoadedLevelNumber++;
                SceneManager.UnloadSceneAsync(currentlyLoadedLevel);
                SceneManager.LoadScene(currentlyLoadedLevel.buildIndex+1, LoadSceneMode.Additive);
            }
        }


        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            currentlyLoadedLevel = scene;
            SceneManager.SetActiveScene(scene);
        }
    }
}

