using UnityEngine;
using UnityEngine.Events;

namespace Tanks.Game
{
    public abstract class Level : MonoBehaviour
    {
        public UnityEvent OnWin;
        public UnityEvent OnLose;
        public UnityEvent<GameObject> OnPlayerSpawned;

        public abstract void SpawnLevel(LevelDetails levelDetails);

        public abstract void SpawnTanks();

        public abstract void Begin();

        public virtual int GetNumberOfEnemyTanks() { return 0; }
    }
}


