using System.Collections.Generic;
using UnityEngine;
using Tanks.Level;
using Tanks.Tank;
using System.Collections;

namespace Tanks.Game
{
    public class DebugWaveLevel : Level
    {
        [SerializeField] private TankSpawnData playerSpawnData;
        [SerializeField] private TankSpawnData[] enemySpawnData;

        private GameObject player;
        private TankController playerTankController;

        private TankSpawner playerSpawner;
        private TankSpawner enemySpawner;
        private List<GameObject> enemies = new List<GameObject>();
        private List<TankController> enemyTankControllers = new List<TankController>();
        private LevelDetails levelDetails;

        public void Awake()
        {
            playerSpawner = new TankSpawner(new TankSpawnData[] { playerSpawnData });
            enemySpawner = new TankSpawner(enemySpawnData);
        }

        public override void Begin()
        {
            // Give control back to the player and enemy tanks
            player.GetComponent<Player.PlayerController>().SetControl(true);
            foreach (TankController controller in enemyTankControllers)
            {
                controller.SetControl(true);
            }
            StartCoroutine(GameUpdate());
        }

        private IEnumerator GameUpdate()
        {
            bool gameIsOver = false;

            while(gameIsOver == false)
            {
                if (playerTankController.IsDead())
                {
                    yield return new WaitForSeconds(5.0f);
                    SpawnPlayer(levelDetails);
                    yield return null;
                }

                bool allEnemiesDead = true;
                for (int i = 0; i < enemyTankControllers.Count; i++)
                {
                    if(enemyTankControllers[i] && !enemyTankControllers[i].IsDead())
                    {
                        allEnemiesDead = false;
                    }
                }

                if (allEnemiesDead)
                {
                    yield return new WaitForSeconds(5.0f);
                    SpawnEnemies(levelDetails);
                }

                yield return null;
            }

            Debug.Log("Game is Over!");
        }

        private void SpawnPlayer(LevelDetails level)
        {
            player = playerSpawner.Spawn(0);
            playerTankController = player.GetComponent<Player.PlayerController>();
            playerTankController.SetControl(false);
            OnPlayerSpawned.Invoke(player);

            level.playerTank = playerTankController;
        }

        private void SpawnEnemies(LevelDetails level)
        {
            enemies.Clear();
            enemyTankControllers.Clear();
            enemySpawner.SpawnAll(ref enemies);

            for (int i = 0; i < enemies.Count; i++)
            {
                TankController aiController = enemies[i].GetComponent<AI.AITankController>();
                aiController.AssignLevelDetails(level);
                aiController.SetControl(false);
                enemyTankControllers.Add(aiController);
            }
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        public override void SpawnLevel(LevelDetails levelDetails)
        {
            this.levelDetails = levelDetails;
        }

        public override void SpawnTanks()
        {
            SpawnPlayer(levelDetails);
            SpawnEnemies(levelDetails);
        }
    }
}


