using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tanks.Level;
using Tanks.Tank;

namespace Tanks.Game
{
    public class SurvivalLevel : Level
    {
        [SerializeField] private TankSpawner playerSpawner;
        [SerializeField] private TankSpawner enemySpawner;

        private GameObject player;
        private Health playerHealth;

        private List<GameObject> enemies = new List<GameObject>();
        private List<Health> enemiesHealth = new List<Health>();

        public override void SpawnLevel(LevelDetails levelDetails)
        {
            throw new System.NotImplementedException("Implement passing level details");
        }

        public override void SpawnTanks()
        {
            SpawnPlayer();
            SpawnEnemies();
        }

        public override void Begin()
        {
            StartCoroutine(GameUpdate());
            player.GetComponent<Player.PlayerController>().SetControl(true);
        }

        private IEnumerator GameUpdate()
        {
            bool gameIsOver = false;

            while (gameIsOver == false)
            {
                if (playerHealth.IsDead())
                {
                    OnLose.Invoke();
                    gameIsOver = true;
                    yield return null;
                }

                bool allEnemiesDead = true;
                for (int i = 0; i < enemiesHealth.Count; i++)
                {
                    if (enemiesHealth[i] && !enemiesHealth[i].IsDead())
                    {
                        allEnemiesDead = false;
                    }
                }

                if (allEnemiesDead)
                {
                    OnWin.Invoke();
                    gameIsOver = true;
                }

                yield return null;
            }

            Debug.Log("Game is Over!");
        }

        private void SpawnPlayer()
        {
            player = playerSpawner.Spawn(0);
            playerHealth = player.GetComponent<Health>();
            player.GetComponent<Player.PlayerController>().SetControl(false);
            OnPlayerSpawned.Invoke(player);
        }

        private void SpawnEnemies()
        {
            enemies.Clear();
            enemiesHealth.Clear();
            enemySpawner.SpawnAll(ref enemies);

            for (int i = 0; i < enemies.Count; i++)
            {
                enemiesHealth.Add(enemies[i].GetComponent<Health>());
            }
        }

        public override int GetNumberOfEnemyTanks()
        {
            return enemySpawner.NumberOfTanks();
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }

}

