using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Animator))]
public class LevelUIController : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI introMissionNumberText;
    [SerializeField] private TMPro.TextMeshProUGUI introPlayerLivesText;
    [SerializeField] private TMPro.TextMeshProUGUI introEnemyNumberText;
    [SerializeField] private TMPro.TextMeshProUGUI startMissionNumberText;
    [SerializeField] private TMPro.TextMeshProUGUI startEnemyNumberText;

    private Animator anim;

    private int ANIM_LEVEL_ENTRY = Animator.StringToHash("LevelEntry");
    private int ANIM_LEVEL_ENTRY_FULL = Animator.StringToHash("Base Layer.LevelEntry");
    private int ANIM_LEVEL_START = Animator.StringToHash("LevelStart");
    private int ANIM_LEVEL_START_FULL = Animator.StringToHash("Base Layer.LevelStart");
    private int ANIM_LEVEL_COMPLETE = Animator.StringToHash("LevelComplete");
    private int ANIM_LEVEL_COMPLETE_FULL = Animator.StringToHash("Base Layer.LevelComplete");
    private int ANIM_LEVEL_LOST = Animator.StringToHash("LevelLost");
    private int ANIM_LEVEL_LOST_FULL = Animator.StringToHash("Base Layer.LevelLost");

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public IEnumerator BeginLevel(int missionNumber, int playerLives, int numberOfEnemies)
    {
        introMissionNumberText.text = "Mission " + missionNumber.ToString();
        introPlayerLivesText.text = playerLives.ToString();
        introEnemyNumberText.text = "Enemy tanks: " + numberOfEnemies.ToString();
        startMissionNumberText.text = missionNumber.ToString();
        startEnemyNumberText.text = numberOfEnemies.ToString();

        anim.SetTrigger(ANIM_LEVEL_ENTRY);

        // wait until we are in the right animation state
        while(anim.GetCurrentAnimatorStateInfo(0).fullPathHash != ANIM_LEVEL_ENTRY_FULL)
        {
            yield return null;
        }

        // wait for the animation to do a full loop
        while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
        {
            yield return null;
        }

        anim.SetTrigger(ANIM_LEVEL_START);

        // wait until we are in the right animation state
        //while (anim.GetCurrentAnimatorStateInfo(0).fullPathHash != ANIM_LEVEL_START_FULL)
        //{
        //    yield return null;
        //}

        //// wait for the animation to do a full loop
        //while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
        //{
        //    yield return null;
        //}
    }

    public IEnumerator LevelComplete()
    {
        anim.SetTrigger(ANIM_LEVEL_COMPLETE);

        // wait until we are in the right animation state
        while (anim.GetCurrentAnimatorStateInfo(0).fullPathHash != ANIM_LEVEL_COMPLETE_FULL)
        {
            yield return null;
        }

        // wait for the animation to do a full loop
        while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
        {
            yield return null;
        }
    }

    public IEnumerator LevelLost()
    {
        anim.SetTrigger(ANIM_LEVEL_LOST);

        // wait until we are in the right animation state
        while (anim.GetCurrentAnimatorStateInfo(0).fullPathHash != ANIM_LEVEL_LOST_FULL)
        {
            yield return null;
        }

        // wait for the animation to do a full loop
        while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
        {
            yield return null;
        }
    }
}
