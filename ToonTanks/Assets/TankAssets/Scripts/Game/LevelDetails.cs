using UnityEngine;

namespace Tanks.Game
{
    [System.Serializable]
    public class LevelDetails
    {
        public Vector2 levelMinBounds;
        public Vector2 levelMaxBounds;
        public Tank.TankController playerTank;

        public RicochetWallManager ricochetWallManager;

        public float MaxDistance => Mathf.Max(levelMaxBounds.y - levelMinBounds.y, levelMaxBounds.x - levelMinBounds.x);
    }
}


