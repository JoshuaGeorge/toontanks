using System.Collections;
using UnityEngine;

namespace Tanks.Game
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private LevelUIController uiController;
        [SerializeField] private Level level;
        [SerializeField] private LevelDetails levelDetails;

        private IEnumerator Start()
        {
            levelDetails.ricochetWallManager = new RicochetWallManager(FindObjectsOfType<Wall>());
            level.SpawnLevel(levelDetails);
            level.SpawnTanks();

            int missionNumber = GameManager.Instance != null ? GameManager.Instance.GetLevelNumber() : 0;
            int numberOfEnemies = level.GetNumberOfEnemyTanks();
            int playerLives = GameManager.Instance != null ? GameManager.Instance.GetPlayerLives() : 0;

            Coroutine levelStart = StartCoroutine(uiController.BeginLevel(missionNumber, playerLives, numberOfEnemies));
            yield return levelStart;

            level.OnWin.AddListener(OnLevelComplete);
            level.OnLose.AddListener(OnLevelLost);

            level.Begin();
        }

        private void OnLevelComplete()
        {
            StartCoroutine(LevelComplete());
        }

        private void OnLevelLost()
        {
            StartCoroutine(LevelLost());
        }

        private IEnumerator LevelLost()
        {
            yield return new WaitForSeconds(3.0f);
            level.SpawnTanks();

            int missionNumber = GameManager.Instance != null ? GameManager.Instance.GetLevelNumber() : 0;
            int numberOfEnemies = level.GetNumberOfEnemyTanks();
            int playerLives = GameManager.Instance != null ? GameManager.Instance.GetPlayerLives() : 0;

            Coroutine levelStart = StartCoroutine(uiController.BeginLevel(missionNumber, playerLives, numberOfEnemies));
            yield return levelStart;

            level.Begin();
        }

        private IEnumerator LevelComplete()
        {
            yield return new WaitForSeconds(3.0f);

            Coroutine levelComplete = StartCoroutine(uiController.LevelComplete());
            yield return levelComplete;

            Debug.Log("Level Complete");
            if (GameManager.Instance)
            {
                GameManager.Instance.LoadNextLevel();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Vector2 center = (levelDetails.levelMaxBounds + levelDetails.levelMinBounds) / 2.0f;
            Vector2 size = levelDetails.levelMaxBounds - levelDetails.levelMinBounds;
            Gizmos.DrawWireCube(new Vector3(center.x, transform.position.y, center.y), new Vector3(size.x, 20.0f, size.y));
        }
    }
}

