using Tanks.Game;
using Tanks.Tank;
using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New ShootAtInterval", menuName = "Tanks/AI/ShouldShoot/ShootAtInterval")]
    public class ShootAtIntervalData : ShouldShoot
    {
        [SerializeField]
        public Range ShootCooldownRange;

        public override AITankAction<bool> Create()
        {
            return new ShootAtInterval(this);
        }
    }

    internal class ShootAtInterval : AITankAction<bool>
    {
        private ShootAtIntervalData data;

        private float currentShootCooldown;
        private float lastShotTime;

        public ShootAtInterval(ShootAtIntervalData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, LevelDetails level)
        {
            currentShootCooldown = data.ShootCooldownRange.Random();
            lastShotTime = Time.time;
        }

        public bool UpdateAction(float time, float deltaTime, ref bool value)
        {
            if (time - lastShotTime > currentShootCooldown)
            {
                currentShootCooldown = data.ShootCooldownRange.Random();
                lastShotTime = time;
                value = true;
                return true;
            }

            value = false;
            return false;
        }

        public void UpdateTarget(TankController target)
        {
        }

        public void OnDebugDrawGizmos()
        {
        }
    }
}

