using UnityEngine;
using Tanks.Tank;
using Tanks.Game;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New ShootWhenAimedAtTarget", menuName = "Tanks/AI/ShouldShoot/ShootWhenAimedAtTarget")]
    public class ShootWhenAimedAtTargetData : ShouldShoot
    {
        public override AITankAction<bool> Create()
        {
            return new ShootWhenAimedAtTarget();
        }
    }

    internal class ShootWhenAimedAtTarget : AITankAction<bool>
    {
        private AITankController controller;
        private TankController playerTank;

        public void Initialize(AITankController controller, LevelDetails level)
        {
            this.controller = controller;
        }

        public bool UpdateAction(float time, float deltaTime, ref bool value)
        {
            if(playerTank == null) return false;

            bool isPlayerAlive = playerTank.IsDead() == false;
            bool turretIsAimedAtTarget = controller.IsTurretAimedAtTarget();
            value = turretIsAimedAtTarget && isPlayerAlive;
            return turretIsAimedAtTarget && isPlayerAlive;
        }

        public void UpdateTarget(TankController target)
        {
            playerTank = target;
        }

        public void OnDebugDrawGizmos()
        {
        }
    }
}
