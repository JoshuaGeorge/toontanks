using UnityEngine;

namespace Tanks.AI
{
    public abstract class ShouldShoot : ScriptableObject
    {
        public abstract AITankAction<bool> Create();
    }
}

