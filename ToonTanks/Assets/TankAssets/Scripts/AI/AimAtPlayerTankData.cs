using UnityEngine;
using Tanks.Game;
using Tanks.Tank;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New AimAtPlayerTank", menuName = "Tanks/AI/CalculateAimPosition/AimAtPlayerTank", order = 1)]
    public class AimAtPlayerTankData : CalculateAimPosition
    {
        [SerializeField] public float Range = 20.0f;
        [SerializeField] public bool UsePredictiveAiming = false;
        [SerializeField] public float PredictiveAimingHeading = 2.0f;
        [SerializeField] public bool UseXray = false;

        [Range(0.0f, 15.0f)]
        [SerializeField] public float AimOffsetAmount = 1.0f;

        public float MoveBetweenOffsetsSpeed = 5.0f;

        public override AITankAction<Vector3> Create()
        {
            return new AimAtPlayerTank(this);
        }
    }

    internal class AimAtPlayerTank : AITankAction<Vector3>
    {
        private AimAtPlayerTankData data;
        private AITankController controller;
        private TankController playerTank;
        private LayerMask layerMask;

        public AimAtPlayerTank(AimAtPlayerTankData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, LevelDetails level)
        {
            this.controller = controller;
            layerMask = LayerMask.GetMask("Wall", "Obstacle");
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector3 value)
        {
            if (playerTank == null) return false;

            Vector3 playerPos = playerTank.GetPosition();
            Vector3 currentPos = controller.GetPosition();

            Vector3 toPlayer = playerPos - currentPos;
            float distanceToPlayer = toPlayer.magnitude;

            // if player within range
            if (distanceToPlayer < data.Range)
            {
                // since we are not using x-ray vision, we need to check if there are any obstacles in the AI's line of sight
                if (data.UseXray == false)
                {
                    // if we hit any walls or obstacles between us and the player, then our visibility is blocked and thus we should not aim at the player
                    if (Physics.Raycast(currentPos, toPlayer, distanceToPlayer, layerMask))
                    {
                        return false;
                    }
                }

                if (data.UsePredictiveAiming)
                {
                    playerPos += playerTank.GetVelocity() * data.PredictiveAimingHeading;
                    toPlayer = playerPos - currentPos;
                }

                Vector3 aimOffset = Vector3.Cross(toPlayer.normalized, Vector3.up);
                aimOffset *= data.AimOffsetAmount;

                Vector3 aimPos = Vector3.Lerp(playerPos + aimOffset, playerPos - aimOffset, (Mathf.Sin(time * data.MoveBetweenOffsetsSpeed) + 1) * 0.5f);

                // determine aim position based on accuracy
                value = aimPos;
                return true;
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
            playerTank = target;
        }

        // use sin of time to determine offset position. get offset vector from cross product between up vector and aim position towards player. then lerp between position and negative of cross product result.
        // finally, use sin of time as the input into the lerp. 

        public void OnDebugDrawGizmos()
        {
            if (controller)
            {
                Gizmos.DrawWireSphere(controller.GetPosition(), data.Range);
            }
        }
    }
}