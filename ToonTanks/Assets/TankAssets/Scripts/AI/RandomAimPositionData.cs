using Tanks.Tank;
using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New RandomAimPosition", menuName = "Tanks/AI/CalculateAimPosition/RandomAimPosition")]
    public class RandomAimPositionData : CalculateAimPosition
    {
        [SerializeField] public float RefreshPositionTime = 5.0f;

        public float AIM_RADIUS = 5.0f;

        public override AITankAction<Vector3> Create()
        {
            return new RandomAimPosition(this);
        }
    }

    internal class RandomAimPosition : AITankAction<Vector3>
    {
        private RandomAimPositionData data;

        private float lastRefreshTime = 0.0f;
        private AITankController controller;

        public RandomAimPosition(RandomAimPositionData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            this.controller = controller;
            lastRefreshTime = Time.time;
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector3 value)
        {
            if ((time - lastRefreshTime) > data.RefreshPositionTime)
            {
                value = controller.GetPosition() + (Random.onUnitSphere * data.AIM_RADIUS);
                lastRefreshTime = time;
                return true;
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
        }


        public void OnDebugDrawGizmos()
        {
        }
    }
}

