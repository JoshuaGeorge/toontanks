namespace Tanks.AI
{
    [System.Serializable]
    public struct Range
    {
        public float min;
        public float max;

        public float Random()
        {
            return UnityEngine.Random.Range(min, max);
        }
    }

    public interface AITankAction<T>
    {
        void Initialize(AITankController controller, Game.LevelDetails level);
        /// <returns> Returns true if the value was changed </returns>
        bool UpdateAction(float time, float deltaTime, ref T value);
        void UpdateTarget(Tank.TankController target);
        void OnDebugDrawGizmos();
    }
}
