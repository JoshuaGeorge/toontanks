using Tanks.Tank;
using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New ClickToMovePosition", menuName = "Tanks/AI/CalculateMovePosition/ClickToMovePosition")]
    public class ClickToMovePositionData : CalculateMovePosition
    {
        [SerializeField]
        public GameObject CursorCanvasPrefab;

        public override AITankAction<Vector2> Create()
        {
            return new ClickToMovePosition(this);
        }
    }

    internal class ClickToMovePosition : AITankAction<Vector2>
    {
        private ClickToMovePositionData data;

        private Player.AimCursor aimCursor;

        public ClickToMovePosition(ClickToMovePositionData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            aimCursor = GameObject.Instantiate(data.CursorCanvasPrefab).GetComponent<Player.AimCursor>();
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector2 value)
        {
            aimCursor.SetPosition(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 cursorPosition = aimCursor.GetWorldSpaceCursorPosition();
                value = new Vector2(cursorPosition.x, cursorPosition.z);

                return true;
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
        }

        public void OnDebugDrawGizmos()
        {
        }
    }
}

