using UnityEngine;

namespace Tanks.AI
{
    public abstract class CalculateDirection : ScriptableObject
    {
        public abstract AITankAction<Vector2> Create();
    }
}
