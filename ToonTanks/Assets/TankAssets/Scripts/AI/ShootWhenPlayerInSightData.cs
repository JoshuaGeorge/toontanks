using UnityEngine;
using Tanks.Tank;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New ShootWhenPlayerInSight", menuName = "Tanks/AI/ShouldShoot/ShootWhenPlayerInSight")]
    public class ShootWhenPlayerInSightData : ShouldShoot
    {
        [SerializeField] public float SphereCastRadius = 1.0f;
        [Range(5.0f, 100.0f)]
        [SerializeField] public float SightRange = 50.0f;

        public override AITankAction<bool> Create()
        {
            return new ShootWhenPlayerInSight(this);
        }
    }

    internal class ShootWhenPlayerInSight : AITankAction<bool>
    {
        private ShootWhenPlayerInSightData data;

        private AITankController controller;
        private TankController playerTank;
        private RaycastHit[] hitResults;
        private LayerMask layerMask;

        public ShootWhenPlayerInSight(ShootWhenPlayerInSightData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            this.controller = controller;
            hitResults = new RaycastHit[4];
            layerMask = LayerMask.GetMask("Players", "Wall", "Obstacle");
        }

        public bool UpdateAction(float time, float deltaTime, ref bool value)
        {
            if(playerTank == null) return false;

            if (!playerTank.IsDead() && controller.CanShootProjectile())
            {
                if (IsAPlayerInSight())
                {
                    value = true;
                    return true;
                }
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
            playerTank = target;
        }

        public void OnDebugDrawGizmos()
        {
            if (controller)
            {
                Color originalColour = Gizmos.color;
                Gizmos.DrawWireSphere(controller.GetPosition(), data.SightRange);
                Gizmos.color = (IsAPlayerInSight()) ? Color.green : Color.red;
                Vector3 sphereCastOffset = controller.GetTurretTransform().right * (data.SphereCastRadius * 0.5f);
                Gizmos.DrawLine(controller.GetTurretTransform().position, controller.GetTurretAimDirection() * data.SightRange);
                Gizmos.DrawLine(controller.GetTurretTransform().position - sphereCastOffset, controller.GetTurretAimDirection() * data.SightRange - sphereCastOffset);
                Gizmos.DrawLine(controller.GetTurretTransform().position + sphereCastOffset, controller.GetTurretAimDirection() * data.SightRange + sphereCastOffset);
                Gizmos.color = originalColour;
            }
        }

        private bool IsAPlayerInSight()
        {
            // Do a sphere cast in the direction the AI is aiming
            int hitCount = Physics.SphereCastNonAlloc(controller.GetTurretTransform().position, data.SphereCastRadius, controller.GetTurretAimDirection(), hitResults, data.SightRange, layerMask);

            float playerDistance = float.PositiveInfinity;
            float closestObjectDistance = float.PositiveInfinity;

            for (int i = 0; i < hitCount; i++)
            {
                if (hitResults[i].transform.CompareTag("Player"))
                {
                    playerDistance = hitResults[i].distance;
                }
                else
                {
                    // if we have seen an enemy tank (including ourselves which is almost always the case) we should ignore it. 
                    if (hitResults[i].transform.CompareTag("Enemy"))
                    {
                        continue;
                    }

                    if (hitResults[i].distance < closestObjectDistance)
                    {
                        closestObjectDistance = hitResults[i].distance;
                    }
                }
            }

            // player is only in sight if they are the closest object
            return playerDistance < closestObjectDistance;
        }
    }
}

