using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New MoveToRandomPosition", menuName = "Tanks/AI/CalculateMovePosition/MoveToRandomPosition")]
    public class MoveToRandomPositionData : CalculateMovePosition
    {
        [SerializeField] public Range FindNewPositionCooldown;

        public override AITankAction<Vector2> Create()
        {
            return new MoveToRandomPosition(this);
        }
    }

    internal class MoveToRandomPosition : AITankAction<Vector2>
    {
        private MoveToRandomPositionData data;

        private AITankController controller;
        private Game.LevelDetails level;
        private float currentCooldownTime;
        private float lastUpdatedPositionTime;
        private Vector2 currentTargetPosition;

        public MoveToRandomPosition(MoveToRandomPositionData data)
        {
            this.data = data;
        } 

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            this.controller = controller;
            this.level = level;
            currentCooldownTime = data.FindNewPositionCooldown.Random();
            lastUpdatedPositionTime = Time.time;
            currentTargetPosition = controller.GetPosition2D();
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector2 value)
        {
            // is it time to find a new position?
            if ((time - lastUpdatedPositionTime) > currentCooldownTime)
            {
                currentTargetPosition = FindRandomPositionInLevel(level);
                lastUpdatedPositionTime = time;
                currentCooldownTime = data.FindNewPositionCooldown.Random();

                value = currentTargetPosition;
                return true;
            }

            return false;
        }

        public void UpdateTarget(Tank.TankController target)
        {
        }

        public void OnDebugDrawGizmos()
        {
            if (controller)
            {
                Gizmos.DrawWireSphere(new Vector3(currentTargetPosition.x, 0.0f, currentTargetPosition.y), 1.0f);
            }
        }

        private static Vector2 FindRandomPositionInLevel(Game.LevelDetails levelDetails)
        {
            return new Vector2(Random.Range(levelDetails.levelMinBounds.x, levelDetails.levelMaxBounds.x), Random.Range(levelDetails.levelMinBounds.y, levelDetails.levelMaxBounds.y));
        }
    }
}

