using Tanks.Game;
using UnityEngine;
using Tanks.Tank;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New RicochetAimPosition", menuName = "Tanks/AI/CalculateAimPosition/RicochetAimPosition", order = 1)]
    public class RicochetAimPositionData : CalculateAimPosition
    {
        public override AITankAction<Vector3> Create()
        {
            return new RicochetAimPosition();
        }
    }

    internal class RicochetAimPosition : AITankAction<Vector3>
    {
        private AITankController controller;
        private TankController playerTank;
        private RicochetWallManager wallManager;
        private Wall wall;

        public void Initialize(AITankController controller, LevelDetails level)
        {
            this.controller = controller;
            wallManager = level.ricochetWallManager;
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector3 value)
        {
            if(playerTank == null) return false;

            Vector2 ricochetAimDir = wallManager.GetRicochetDirection(controller.GetPosition(), playerTank.transform, out wall);

            if (wall != null)
            {
                value = new Vector3(ricochetAimDir.x, controller.GetPosition().y, ricochetAimDir.y);
                return true;
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
            playerTank = target;
        }

        public void OnDebugDrawGizmos()
        {
            if (controller == null || playerTank == null || wall == null) return;

            Vector3 position = controller.GetTurretTransform().position;
            position.y = 0.0f;
            Vector2 position2D = new Vector2(position.x, position.z);
            Vector2 playerPosition = playerTank.GetPosition2D();
            Vector2 reflectedTargetPos = new Vector2(0.0f, 0.0f);

            if (wall.IsVisibleFromPositiveX(position2D) && wall.IsVisibleFromPositiveX(playerPosition))
            {
                Gizmos.color = Color.red;
                reflectedTargetPos = wall.GetReflectedPositionFromPositiveX(playerPosition);
            }

            if (wall.IsVisibleFromPositiveZ(position2D) && wall.IsVisibleFromPositiveZ(playerPosition))
            {
                Gizmos.color = Color.blue;
                reflectedTargetPos = wall.GetReflectedPositionFromPositiveZ(playerPosition);
            }

            if (wall.IsVisibleFromNegativeX(position2D) && wall.IsVisibleFromNegativeX(playerPosition))
            {
                Gizmos.color = Color.yellow;
                reflectedTargetPos = wall.GetReflectedPositionFromNegativeX(playerPosition);
            }

            if (wall.IsVisibleFromNegativeZ(position2D) && wall.IsVisibleFromNegativeZ(playerPosition))
            {
                Gizmos.color = Color.cyan;
                reflectedTargetPos = wall.GetReflectedPositionFromNegativeZ(playerPosition);
            }

            Gizmos.DrawWireCube(new Vector3(reflectedTargetPos.x, 1.0f, reflectedTargetPos.y), Vector3.one);

            RaycastHit hit;
            Vector2 toWall = reflectedTargetPos - position2D;
            Physics.Raycast(position, new Vector3(toWall.x, 0.0f, toWall.y), out hit);
            if (hit.collider != null)
            {
                Gizmos.color = Color.red;
                Vector3 hitPoint = new Vector3(hit.point.x, 0.0f, hit.point.z);
                Gizmos.DrawLine(position, hitPoint);
                Vector2 reflectRay = Vector2.Reflect(toWall, new Vector2(hit.normal.x, hit.normal.z));
                Gizmos.DrawLine(hitPoint, hitPoint + new Vector3(reflectRay.x, 0.0f, reflectRay.y));

                Gizmos.color = Color.blue;
                Gizmos.DrawLine(position, new Vector3(reflectedTargetPos.x, 0.0f, reflectedTargetPos.y));
            }
        }
    }
}

