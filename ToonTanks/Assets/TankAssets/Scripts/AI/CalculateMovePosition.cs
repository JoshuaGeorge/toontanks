using UnityEngine;

namespace Tanks.AI
{
    public abstract class CalculateMovePosition : ScriptableObject
    {
        public abstract AITankAction<Vector2> Create();
    }
}

