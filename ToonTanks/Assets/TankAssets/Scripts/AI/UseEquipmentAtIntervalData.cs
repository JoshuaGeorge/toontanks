using UnityEngine;
using Tanks.Weapons;
using Tanks.Tank;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New UseEquipmentAtInterval", menuName = "Tanks/AI/ShouldUseEquipment/UseEquipmentAtInterval", order = 1)]
    public class UseEquipmentAtIntervalData : ScriptableObject
    {
        [SerializeField]
        public BaseEquipment EquipmentPrefabRef;

        [SerializeField]
        public Range CooldownRange;

        public AITankAction<bool> Create()
        {
            return new UseEquipmentAtInterval(this);
        }
    }

    internal class UseEquipmentAtInterval : AITankAction<bool>
    {
        private UseEquipmentAtIntervalData data;

        private float lastUsedEquipmentTime;
        private float currentCooldownTime;

        public UseEquipmentAtInterval(UseEquipmentAtIntervalData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            controller.Equip(GameObject.Instantiate(data.EquipmentPrefabRef));
            currentCooldownTime = data.CooldownRange.Random();
            lastUsedEquipmentTime = Time.time;
        }


        public bool UpdateAction(float time, float deltaTime, ref bool value)
        {
            if ((time - lastUsedEquipmentTime) > currentCooldownTime)
            {
                lastUsedEquipmentTime = time;
                currentCooldownTime = data.CooldownRange.Random();
                value = true;
                return true;
            }
            return false;
        }

        public void UpdateTarget(TankController target)
        {
        }

        public void OnDebugDrawGizmos()
        {
        }
    }
}

