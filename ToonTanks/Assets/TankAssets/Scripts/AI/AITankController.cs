using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tanks.Tank;
using Tanks.Weapons;
using Tanks.Game;
using Tanks.Interfaces;

namespace Tanks.AI
{
    public class AITankController : TankController
    {
        [SerializeField] private NavMeshAgent agent;
        [Tooltip("Defines how strictly the tank follows the path defined by the navmesh path. Smaller means more strict while larger numbers produce a smoother follow")]
        [Range(0.5f, 10.0f)]
        [SerializeField] private float followPathSmoothness = 0.5f;             // https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-path-following--gamedev-8769
        [SerializeField] private CalculateAimPosition[] calculateAimPositionActions;
        [SerializeField] private ShouldShoot[] shouldShootActions;
        [SerializeField] private CalculateMovePosition[] calculateMovePositionActions;
        [SerializeField] private CalculateDirection[] calculateDirectionActions;
        [SerializeField] private UseEquipmentAtIntervalData[] useEquipmentActions;

        private AITankAction<Vector3>[] calculateAimPositionAction;
        private AITankAction<bool>[] shouldShootAction;
        private AITankAction<Vector2>[] calculateMovePositionAction;
        private AITankAction<Vector2>[] calculateDirectionAction;
        private AITankAction<bool>[] useEquipmentAction;

        private EquipmentSlot equipmentSlot = new EquipmentSlot();

        private Vector3 turretAimPosition;
        private bool shouldShoot;
        private Vector2 targetPosition;
        private bool shouldUseEquipment;
        private NavMeshPath navMeshPath;
        private int currentNavMeshPathIndex;

        // Cached Projectiles Variables
        private Collider[] cachedObstaclesInRange;            // The closest projectiles in a specified range
        private LayerMask obstaclesLayerMask;

#if DEVELOPMENT || UNITY_EDITOR
        private Vector2 targetDir;
        [System.NonSerialized] public bool displayCalculateAimPositionsDebug = true;
        [System.NonSerialized] public bool displayshootActionsDebug = true;
        [System.NonSerialized] public bool displayMovePositionDebug = true;
        [System.NonSerialized] public bool displayCalculateDirectionDebug = true;
        [System.NonSerialized] public bool displayUseEquipmentDebug = true;
#endif



        protected override void Start()
        {
            base.Start();
            navMeshPath = new NavMeshPath();

            agent.updatePosition = false;
            agent.updateRotation = false;

            obstaclesLayerMask = LayerMask.GetMask("Projectile", "StaticObstacle");
            cachedObstaclesInRange = new Collider[10];
        }

        private void Update()
        {
            if (healthComponent.IsDead()) return;

            float dt = Time.deltaTime;
            float currentTime = Time.time;
            Vector2 targetDirection = new Vector2(0.0f, 0.0f);

            if (IsAllowedToShoot())
            {
                ProcessAIAction<Vector3>(calculateAimPositionAction, ref turretAimPosition, currentTime, dt);
            }

            ProcessAIAction<bool>(shouldShootAction, ref shouldShoot, currentTime, dt, false);
            bool movePositionChanged = ProcessAIAction<Vector2>(calculateMovePositionAction, ref targetPosition, currentTime, dt, false);
            ProcessAIAction<Vector2>(calculateDirectionAction, ref targetDirection, currentTime, dt);
            ProcessAIAction<bool>(useEquipmentAction, ref shouldUseEquipment, currentTime, dt);


            turretComponent.LookAt(turretAimPosition);
            if (shouldShoot)
            {
                Shoot();
            }
            if (movePositionChanged)
            {
                currentNavMeshPathIndex = 0;
                agent.nextPosition = transform.position;
                // use the navmesh agent to calculate a path.
                agent.CalculatePath(new Vector3(targetPosition.x, transform.position.y, targetPosition.y), navMeshPath);
            }

            movementComponent.Move(targetDirection.normalized);
            if (shouldUseEquipment)
            {
                shouldUseEquipment = false;
                equipmentSlot.TryUseEquipment();
            }

#if DEVELOPMENT || UNITY_EDITOR
            targetDir = targetDirection.normalized;
#endif
        }

        public override void AssignLevelDetails(LevelDetails levelDetails)
        {
            InitializeActions(levelDetails);
        }

        public override void Equip(IEquipment equipment)
        {
            equipment.OnEquipped(transform);
            equipmentSlot.SetEquipment(equipment);
        }

        public NavigableObstacle GetMostThreateningObstacle(float maxRange, float headingAngleRadians, NavigableObstacle.ObstacleType obstacleType)
        {
            float closestDistanceSqr = Mathf.Infinity;
            NavigableObstacle closestObstacle = null;

            int count = Physics.OverlapSphereNonAlloc(GetPosition(), maxRange, cachedObstaclesInRange, obstaclesLayerMask);

            for (int i = 0; i < count; i++)
            {
                NavigableObstacle obstacle = cachedObstaclesInRange[i].GetComponent<NavigableObstacle>();
                if (obstacle)
                {
                    if (IsObstacleAThreat(obstacle, headingAngleRadians, obstacleType))
                    {
                        Vector3 playerToProjectile = GetPosition() - obstacle.transform.position;
                        float distanceSqr = playerToProjectile.sqrMagnitude;
                        if (distanceSqr < closestDistanceSqr)
                        {
                            closestDistanceSqr = distanceSqr;
                            closestObstacle = obstacle;
                        }
                    }
                }
            }

            return closestObstacle;
        }

        public Vector2 GetNextTargetInPath()
        {
            if (navMeshPath != null && navMeshPath.status == NavMeshPathStatus.PathComplete)
            {
                Vector3[] path = navMeshPath.corners;
                Vector2 targetPosInPath = new Vector2(path[currentNavMeshPathIndex].x, path[currentNavMeshPathIndex].z);

                float distanceFromTargetSqr = (GetPosition2D() - targetPosInPath).sqrMagnitude;

                if (distanceFromTargetSqr < followPathSmoothness && currentNavMeshPathIndex < (path.Length - 1))
                {
                    Vector3 nextPosInPath = path[++currentNavMeshPathIndex];
                    return new Vector2(nextPosInPath.x, nextPosInPath.z);
                }

                return targetPosInPath;
            }

            return GetPosition2D();
        }

        public bool IsTurretAimedAtTarget()
        {
            Vector3 targetDirection = turretAimPosition - GetPosition();
            targetDirection.Normalize();
            Vector3 currentDirection = GetTurretAimDirection();
            return Vector3.Dot(targetDirection, currentDirection) > 0.95f;
        }

        private void InitializeActions(LevelDetails level)
        {
            calculateAimPositionAction = new AITankAction<Vector3>[calculateAimPositionActions.Length];
            for (int i = 0; i < calculateAimPositionActions.Length; i++)
            {
                calculateAimPositionAction[i] = calculateAimPositionActions[i].Create();
                calculateAimPositionAction[i].Initialize(this, level);
            }

            shouldShootAction = new AITankAction<bool>[shouldShootActions.Length];
            for (int i = 0; i < shouldShootActions.Length; i++)
            {
                shouldShootAction[i] = shouldShootActions[i].Create();
                shouldShootAction[i].Initialize(this, level);
            }

            calculateMovePositionAction = new AITankAction<Vector2>[calculateMovePositionActions.Length];
            for (int i = 0; i < calculateMovePositionActions.Length; i++)
            {
                calculateMovePositionAction[i] = calculateMovePositionActions[i].Create();
                calculateMovePositionAction[i].Initialize(this, level);
            }

            calculateDirectionAction = new AITankAction<Vector2>[calculateDirectionActions.Length];
            for (int i = 0; i < calculateDirectionActions.Length; i++)
            {
                calculateDirectionAction[i] = calculateDirectionActions[i].Create();
                calculateDirectionAction[i].Initialize(this, level);
            }

            useEquipmentAction = new AITankAction<bool> [useEquipmentActions.Length];
            for (int i = 0; i < useEquipmentActions.Length; i++)
            {
                useEquipmentAction[i] = useEquipmentActions[i].Create();
                useEquipmentAction[i].Initialize(this, level);
            }
        }

        private bool ProcessAIAction<T>(IEnumerable<AITankAction<T>> actionList, ref T updatedValue, float currentTime, float deltaTime, bool breakOnFirstUpdate = true)
        {
            bool valueWasUpdated = false;

            foreach (AITankAction<T> action in actionList)
            {
                valueWasUpdated |= action.UpdateAction(currentTime, deltaTime, ref updatedValue);
                if (breakOnFirstUpdate && valueWasUpdated)
                {
                    return true;
                }
            }

            return valueWasUpdated;
        }

        private void UpdatePlayerTarget(TankController target)
        {
            for (int i = 0; i < calculateAimPositionAction.Length; i++)
            {
                calculateAimPositionAction[i].UpdateTarget(target);
            }

            for (int i = 0; i < shouldShootAction.Length; i++)
            {
                shouldShootAction[i].UpdateTarget(target);
            }

            for (int i = 0; i < calculateMovePositionAction.Length; i++)
            {
                calculateMovePositionAction[i].UpdateTarget(target);
            }

            for (int i = 0; i < calculateDirectionAction.Length; i++)
            {
                calculateDirectionAction[i].UpdateTarget(target);
            }

            for (int i = 0; i < useEquipmentAction.Length; i++)
            {
                useEquipmentAction[i].UpdateTarget(target);
            }
        }

        private bool IsObstacleAThreat(NavigableObstacle obstacle, float headingAngleRadians, NavigableObstacle.ObstacleType obstacleType)
        {
            if(obstacleType == obstacle.Type)
            {
                switch (obstacle.Type)
                {
                    case NavigableObstacle.ObstacleType.Stationary:
                        {
                            float distance = (GetPosition2D() - obstacle.Position2D).magnitude;
                            Vector2 heading = GetPosition2D() + (GetVelocity2D() * 3.0f);
                            float distanceFromHeading = (heading - obstacle.Position2D).magnitude;

                            return distance < obstacle.AvoidanceRange || distanceFromHeading < obstacle.AvoidanceRange;
                        }
                    case NavigableObstacle.ObstacleType.Projectile:
                        {
                            Vector3 playerToProjectile = GetPosition() - obstacle.Position;
                            Vector3 projectileVelocity = obstacle.Velocity;

                            float dot = Vector3.Dot(playerToProjectile.normalized, projectileVelocity.normalized);
                            float angleInRadians = Mathf.Acos(dot);
                            return angleInRadians < headingAngleRadians;
                        }
                    default:
                        {
                            Debug.LogError(obstacle.name + " has no obstacle type assigned");
                            return false;
                        }
                }
            }
            else
            {
                return false;
            }
        }


#if DEVELOPMENT || UNITY_EDITOR

        public override void OnDrawDebugDisplay(CanvasDebugDisplay debugDisplay)
        {
            debugDisplay.SetDebugText("Target Dir", targetDir.ToString());
            debugDisplay.SetDebugText("RB Vel", GetVelocity2D().ToString());

            //if (displayCalculateDirectionDebug)
            //{
            //    for (int i = 0; i < calculateDirectionActions.Length; i++)
            //    {
            //        calculateDirectionAction[i].OnDrawDebugDisplay(debugDisplay);
            //    }
            //}
        }

        private void OnDrawGizmos()
        {
            if (displayCalculateAimPositionsDebug)
            {
                Gizmos.color = Color.green;
                for (int i = 0; i < calculateAimPositionActions.Length; i++)
                {
                    calculateAimPositionAction[i].OnDebugDrawGizmos();
                }
            }

            if (displayshootActionsDebug)
            {
                Gizmos.color = Color.cyan;
                for (int i = 0; i < shouldShootActions.Length; i++)
                {
                    shouldShootAction[i].OnDebugDrawGizmos();
                }
            }

            if (displayMovePositionDebug)
            {
                Gizmos.color = Color.magenta;
                for (int i = 0; i < calculateMovePositionActions.Length; i++)
                {
                    calculateMovePositionAction[i].OnDebugDrawGizmos();
                }
            }

            if (displayCalculateDirectionDebug)
            {
                Gizmos.color = Color.cyan;
                for (int i = 0; i < calculateDirectionActions.Length; i++)
                {
                    calculateDirectionAction[i].OnDebugDrawGizmos();
                }
            }

            if (displayUseEquipmentDebug)
            {
                Gizmos.color = Color.yellow;
                for (int i = 0; i < useEquipmentActions.Length; i++)
                {
                    useEquipmentAction[i].OnDebugDrawGizmos();
                }
            }


            if (navMeshPath != null)
            {
                Gizmos.color = Color.red;
                Vector3 prevPos = transform.position;
                foreach (var pos in navMeshPath.corners)
                {
                    Gizmos.DrawWireSphere(pos, 1.0f);
                    Gizmos.DrawLine(prevPos, pos);
                    prevPos = pos;
                }

                Vector2 nextTargetPosInPath = GetNextTargetInPath();
                Gizmos.DrawSphere(new Vector3(nextTargetPosInPath.x, transform.position.y, nextTargetPosInPath.y), 1.0f);
            }
        }

#endif

    }
}

