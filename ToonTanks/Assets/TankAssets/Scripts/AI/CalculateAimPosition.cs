using UnityEngine;

namespace Tanks.AI
{
    public abstract class CalculateAimPosition : ScriptableObject
    {
        public abstract AITankAction<Vector3> Create();
    }
}

