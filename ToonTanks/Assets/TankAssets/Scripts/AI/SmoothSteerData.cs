using Tanks.Tank;
using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New SmoothSteer", menuName = "Tanks/AI/CalculateDirection/SmoothSteer")]
    public class SmoothSteerData : CalculateDirection
    {
        [SerializeField] public float MaxSteeringForce = 1.0f;

        public override AITankAction<Vector2> Create()
        {
            return new SmoothSteer(this);
        }
    }

    internal class SmoothSteer : AITankAction<Vector2>
    {
        private SmoothSteerData data;

        private AITankController controller;

        public SmoothSteer(SmoothSteerData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            this.controller = controller;
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector2 value)
        {
            // Algorithm from https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-wander--gamedev-1624
            Vector2 velocity = controller.GetVelocity2D();
            Vector2 desiredVelocity = controller.GetNextTargetInPath() - controller.GetPosition2D();

            if (desiredVelocity.sqrMagnitude < 0.5f)
            {
                value = Vector2.zero;
                return true;
            }

            desiredVelocity = desiredVelocity.normalized * controller.GetMovementSpeed();
            Vector2 steering = desiredVelocity - velocity;
            steering = steering.normalized * data.MaxSteeringForce;

            value = (velocity + steering);
            return true;
        }

        public void UpdateTarget(TankController target)
        {
            throw new System.NotImplementedException();
        }

        public void OnDebugDrawGizmos()
        {
            if (controller)
            {
                Vector2 velocity = controller.GetVelocity2D();
                Gizmos.DrawLine(controller.GetPosition(), controller.GetPosition() + new Vector3(velocity.x, 0.0f, velocity.y));
            }
        }

        public void OnDrawDebugDisplay(CanvasDebugDisplay debugDisplay)
        {
            Vector2 desiredVelocity = controller.GetNextTargetInPath() - controller.GetPosition2D();
            debugDisplay.SetDebugText("Desired Vel", desiredVelocity.sqrMagnitude);
            debugDisplay.SetDebugText("Velocity", controller.GetVelocity2D().ToString());
        }
    }
}

