using UnityEngine;
using Tanks.Tank;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New MoveTowardsPlayer", menuName = "Tanks/AI/CalculateMovePosition/MoveTowardsPlayer")]
    public class MoveTowardsPlayerData : CalculateMovePosition
    {
        [SerializeField] public Range SeekWaitTime;
        [SerializeField] public Range SeekDuration;
        [SerializeField] public float ProximityToPlayerRadius;
        [SerializeField] public float RefreshSeekPositionTime;
        [SerializeField] public bool SneakBehindPlayer = false;        // TODO: Implement this

        public override AITankAction<Vector2> Create()
        {
            return new MoveTowardsPlayer(this);
        }
    }

    internal class MoveTowardsPlayer : AITankAction<Vector2>
    {
        private MoveTowardsPlayerData data;

        private AITankController controller;
        private TankController playerTank;

        private float lastSeekTime;
        private float currentWaitTimeToSeek;
        private float startSeekTime;
        private float currentSeekDuration;

        public MoveTowardsPlayer(MoveTowardsPlayerData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, Game.LevelDetails level)
        {
            this.controller = controller;
            lastSeekTime = Time.time;
            currentWaitTimeToSeek = data.SeekWaitTime.Random();
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector2 value)
        {
            if (playerTank == null) return false;

            // we are in the process of seeking the player
            if (time - startSeekTime < currentSeekDuration)
            {
                if (time - lastSeekTime > data.RefreshSeekPositionTime)
                {
                    lastSeekTime = time;
                    value = FindSeekPositionTowardsTarget(playerTank);
                }
                return true;
            }

            // has the wait time to start seeking elapsed?
            if (time - lastSeekTime > currentWaitTimeToSeek)
            {
                startSeekTime = time;
                currentSeekDuration = data.SeekDuration.Random();
                lastSeekTime = time;
                currentWaitTimeToSeek = data.SeekWaitTime.Random();
                value = FindSeekPositionTowardsTarget(playerTank);
                return true;
            }

            // we are still waiting to seek so return false...
            return false;
        }

        public void UpdateTarget(TankController target)
        {
            playerTank = target;
        }

        public void OnDebugDrawGizmos()
        {
        }

        private Vector2 FindSeekPositionTowardsTarget(TankController targetTank)
        {
            Vector2 toPlayer = targetTank.GetPosition2D() - controller.GetPosition2D();
            Vector2 radiusFromPlayer = -toPlayer.normalized * data.ProximityToPlayerRadius;

            return targetTank.GetPosition2D() + radiusFromPlayer;
        }
    }
}