using Tanks.Game;
using Tanks.Tank;
using UnityEngine;

namespace Tanks.AI
{
    [CreateAssetMenu(fileName = "New AvoidObstacle", menuName = "Tanks/AI/CalculateDirection/AvoidObstacle")]
    public class AvoidObstacleData : CalculateDirection
    {
        [SerializeField]
        public float MaxDetectionRange = 5.0f;

        [SerializeField]
        public NavigableObstacle.ObstacleType ObstacleTypeToAvoid;

        [SerializeField]
        [Range(0.0f, 359.0f)]
        [Tooltip("Only projectiles at this angle from the Tank will be considered a threat and thus avoided")]
        public float MaxAngleToAvoidProjectilesInDegrees = 15.0f;

        public override AITankAction<Vector2> Create()
        {
            return new AvoidObstacle(this);
        }
    }

    internal class AvoidObstacle : AITankAction<Vector2>
    {
        private AvoidObstacleData data;

        private AITankController controller;

        public AvoidObstacle(AvoidObstacleData data)
        {
            this.data = data;
        }

        public void Initialize(AITankController controller, LevelDetails level)
        {
            this.controller = controller;
        }

        public bool UpdateAction(float time, float deltaTime, ref Vector2 value)
        {

            NavigableObstacle navigableObstacle = controller.GetMostThreateningObstacle(data.MaxDetectionRange, Mathf.Deg2Rad * data.MaxAngleToAvoidProjectilesInDegrees, data.ObstacleTypeToAvoid);

            if (navigableObstacle != null)
            {
                switch (navigableObstacle.Type)
                {
                    case NavigableObstacle.ObstacleType.Stationary:
                        value = Avoid(navigableObstacle);
                        break;
                    case NavigableObstacle.ObstacleType.Projectile:
                        value = Evade(navigableObstacle);
                        break;
                }

                return true;
            }

            return false;
        }

        public void UpdateTarget(TankController target)
        {
        }

        public void OnDebugDrawGizmos()
        {
            if (controller)
            {
                NavigableObstacle navigableObstacle = controller.GetMostThreateningObstacle(data.MaxDetectionRange, Mathf.Deg2Rad * data.MaxAngleToAvoidProjectilesInDegrees, data.ObstacleTypeToAvoid);

                if (navigableObstacle != null)
                {
                    Gizmos.DrawWireSphere(controller.GetPosition(), data.MaxDetectionRange);
                    Gizmos.DrawWireSphere(navigableObstacle.Position, 0.5f);
                    Gizmos.DrawLine(controller.GetPosition(), navigableObstacle.Position);
                }
            }
        }

        // Algorithm from https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-collision-avoidance--gamedev-7777
        private Vector2 Avoid(NavigableObstacle obstacle)
        {
            Vector3 projectilePosition = obstacle.Position;
            Vector3 tankPosition = controller.GetPosition();
            Vector3 playerToProjectile = (projectilePosition - tankPosition).normalized;

            Vector3 avoidanceForce = Vector3.Cross(playerToProjectile, Vector3.up);

            // dot product here determines whether the projectile is on the tanks left or right side which is used to flip its avoidance force
            float dot = Vector3.Dot(avoidanceForce, -obstacle.Velocity.normalized);
            return new Vector2(avoidanceForce.x, avoidanceForce.z) * ((dot < 0.0f) ? -1.0f : 1.0f);
        }

        // Algorithm from https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-pursuit-and-evade--gamedev-2946 
        // https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-flee-and-arrival--gamedev-1303
        private Vector2 Evade(NavigableObstacle obstacle)
        {
            Vector2 distance = obstacle.Position2D - controller.GetPosition2D();
            float updatesAhead = distance.magnitude / data.MaxDetectionRange;
            Vector2 obstacleFuturePos = obstacle.Position2D + obstacle.Velocity2D * updatesAhead;

            Vector2 desiredVelocity = controller.GetPosition2D() - obstacleFuturePos;
            return controller.GetVelocity2D() + desiredVelocity;
        }
    }
}

