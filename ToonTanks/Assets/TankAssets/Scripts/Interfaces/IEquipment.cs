using UnityEngine;

namespace Tanks.Interfaces
{
    public interface IEquipment
    {
        void OnEquipped(Transform owner);

        void UseEquipment();
    }
}

