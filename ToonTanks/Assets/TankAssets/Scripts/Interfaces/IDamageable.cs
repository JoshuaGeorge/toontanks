namespace Tanks.Interfaces
{
    public interface IDamageable
    {
        void TakeDamage(Tanks.Structs.Damage damage);
    }
}
