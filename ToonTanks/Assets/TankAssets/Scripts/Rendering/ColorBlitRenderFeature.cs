using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ColorBlitRenderFeature : ScriptableRendererFeature
{
    public RenderPassEvent renderPassEvent;
    public string destinationRTName;
    private ColorBlitPass colorBlitPass;

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        colorBlitPass.Setup(renderer.cameraColorTarget);
        renderer.EnqueuePass(colorBlitPass);
    }

    public override void Create()
    {
        colorBlitPass = new ColorBlitPass(name, destinationRTName, RenderPassEvent.AfterRenderingOpaques);
    }
}

public class ColorBlitPass : ScriptableRenderPass
{
    string profilerTag;

    RenderTargetIdentifier cameraColorTarget;
    RenderTargetIdentifier destinationTarget;
    int destinationTextureId;

    public ColorBlitPass(string profilerTag, string destinationId, RenderPassEvent renderPassEvent)
    {
        this.profilerTag = profilerTag;
        this.renderPassEvent = renderPassEvent;
        destinationTextureId = Shader.PropertyToID(destinationId);
    }

    public void Setup(RenderTargetIdentifier cameraColorTarget)
    {
        this.cameraColorTarget = cameraColorTarget;
    }

    public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
    {
        cmd.GetTemporaryRT(destinationTextureId, cameraTextureDescriptor);
        destinationTarget = new RenderTargetIdentifier(destinationTextureId);
        ConfigureTarget(destinationTextureId);
        ConfigureClear(ClearFlag.Color, Color.clear);
    }

    public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
    {
        CommandBuffer cmd = CommandBufferPool.Get(profilerTag);
        cmd.Blit(cameraColorTarget, destinationTarget);
        cmd.SetRenderTarget(cameraColorTarget);
        context.ExecuteCommandBuffer(cmd);
        cmd.Clear();
        CommandBufferPool.Release(cmd);
    }

    public override void FrameCleanup(CommandBuffer cmd)
    {
        cmd.ReleaseTemporaryRT(destinationTextureId);
    }
}
