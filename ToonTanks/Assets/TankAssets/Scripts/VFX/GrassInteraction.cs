using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class GrassInteraction : MonoBehaviour
{
    public Transform targetTransform;
    public float smoothTime = 1.0f;
    private SphereCollider sphereCollider;


    private float flattenedAmount;  // 0 means we are not flattened at all, 1 means we are completely flattened
    private Quaternion targetRotation;
    private Vector3 targetPosition;

    private void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
        targetPosition = sphereCollider.bounds.center;
    }

    private void Update()
    {
        //transform.position = targetPosition;

        Vector3 originalUpVector = sphereCollider.bounds.center - transform.position;
        Vector3 flattenedUpVector = targetPosition - transform.position;
        originalUpVector.Normalize();
        flattenedUpVector.Normalize();

        targetTransform.rotation = Quaternion.FromToRotation(originalUpVector, flattenedUpVector);
    }

    private void OnTriggerStay(Collider other)
    {
        if(other is SphereCollider)
        {
            SphereCollider otherSphere = (SphereCollider)other;
            float otherSphereRadius = otherSphere.radius * Mathf.Max(other.transform.localScale.x, other.transform.localScale.z);

            Vector3 vectorBetweenColliders = sphereCollider.bounds.center - otherSphere.bounds.center;
            Vector3 pushDirection = vectorBetweenColliders.normalized;
            pushDirection *= (sphereCollider.radius + otherSphereRadius);

            float distanceBetweenColliders = vectorBetweenColliders.magnitude / (sphereCollider.radius + otherSphereRadius);

            targetPosition = (otherSphere.bounds.center + pushDirection);
            flattenedAmount = Mathf.Lerp(1, 0, distanceBetweenColliders);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        targetPosition = transform.position;
    }
}