using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tanks.VFX
{
    public class DissolveMesh : MonoBehaviour
    {
        [SerializeField] private float delay = 0.0f;
        [SerializeField] private float duration = 1.0f;
        [SerializeField] private MeshRenderer[] renderers;
        private int DISSOLVE_THRESHOLD_ID = Shader.PropertyToID("_DissolveThreshold");

        private List<Material> dissolveMaterials;

        void Awake()
        {
            dissolveMaterials = new List<Material>(renderers.Length);

            for (int i = 0; i < renderers.Length; i++)
            {
                for (int j = 0; j < renderers[i].sharedMaterials.Length; j++)
                {
                    dissolveMaterials.Add(renderers[i].materials[j]);
                }
            }
        }

        public void Dissolve()
        {
            StartCoroutine(Dissolving(delay, duration));
        }

        private IEnumerator Dissolving(float delay, float duration)
        {
            // reset threshold to 0
            for (int i = 0; i < dissolveMaterials.Count; i++)
            {
                dissolveMaterials[i].SetFloat(DISSOLVE_THRESHOLD_ID, 0.0f);
            }

            yield return new WaitForSeconds(delay);

            for (float progress = 0; progress < duration; progress += Time.deltaTime)
            {
                float threshold = progress / duration;

                for (int i = 0; i < dissolveMaterials.Count; i++)
                {
                    dissolveMaterials[i].SetFloat(DISSOLVE_THRESHOLD_ID, threshold);
                }
                yield return null;
            }

            for (int i = 0; i < dissolveMaterials.Count; i++)
            {
                dissolveMaterials[i].SetFloat(DISSOLVE_THRESHOLD_ID, 1.05f);
            }
        }
    }

}

