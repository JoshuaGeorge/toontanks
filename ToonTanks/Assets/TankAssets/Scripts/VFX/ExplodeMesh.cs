using System.Collections;
using UnityEngine;

namespace Tanks.VFX
{
    public class ExplodeMesh : MonoBehaviour
    {
        [SerializeField] private Transform explosionOrigin;
        [SerializeField] private float explosionOriginRandomRadius = 0.0f;
        [SerializeField] private Vector2 explosionForceRandomRange;
        [SerializeField] private float explosionRadius;
        [Range(0.0f, 1.0f)]
        [SerializeField] private float upwardsModifierMin;
        [Range(0.0f, 1.0f)]
        [SerializeField] private float upwardsModifierMax;
        [SerializeField] private Rigidbody[] rigidbodies;

        private Transform[] transforms;
        private Vector3[] startingPositions;
        private Quaternion[] startingRotations;

        private void Awake()
        {
            transforms = new Transform[rigidbodies.Length];
            startingPositions = new Vector3[rigidbodies.Length];
            startingRotations = new Quaternion[rigidbodies.Length];

            for (int i = 0; i < rigidbodies.Length; i++)
            {
                transforms[i] = rigidbodies[i].transform;
                startingPositions[i] = transforms[i].position;
                startingRotations[i] = transforms[i].rotation;
            }    
        }

        [ContextMenu("Explode")]
        public void Explode()
        {
            StartCoroutine(Explosion());
        }

        private IEnumerator Explosion()
        {
            Vector3 zeroVector = new Vector3(0.0f, 0.0f, 0.0f);
            for (int i = 0; i < rigidbodies.Length; i++)
            {
                rigidbodies[i].transform.position = startingPositions[i];
                rigidbodies[i].transform.rotation = startingRotations[i];
                rigidbodies[i].velocity = zeroVector;
                rigidbodies[i].angularVelocity = zeroVector;
                rigidbodies[i].isKinematic = true;
            }

            yield return new WaitForFixedUpdate();

            for (int i = 0; i < rigidbodies.Length; i++)
            {
                Vector3 explosionPosition = (explosionOriginRandomRadius > 0) ? explosionOrigin.position + (Random.insideUnitSphere * explosionOriginRandomRadius) : explosionOrigin.position;

                rigidbodies[i].isKinematic = false;
                rigidbodies[i].AddExplosionForce(Random.Range(explosionForceRandomRange.x, explosionForceRandomRange.y), explosionPosition, explosionRadius, Random.Range(upwardsModifierMin, upwardsModifierMax), ForceMode.Impulse);
            }
        }
    }

}

 