using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateColour : MonoBehaviour
{
    [SerializeField] private Color colour01;
    [SerializeField] private Color colour02;
    [SerializeField] private MeshRenderer[] meshRenderers;
    [SerializeField] private float speed = 0.5f;

    private Material[] materials;
    private int SHADER_COLOUR_ID = Shader.PropertyToID("_BaseColor");

    // Start is called before the first frame update
    void Start()
    {
        materials = new Material[meshRenderers.Length];
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            materials[i] = meshRenderers[i].material;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float t = Mathf.PingPong(Time.time * speed, 1.0f);
        Color currentColour = Color.Lerp(colour01, colour02, t);

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].SetColor(SHADER_COLOUR_ID, currentColour);
        }
    }
}
