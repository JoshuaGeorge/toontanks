using UnityEngine;
using UnityEngine.VFX;

namespace Tanks.VFX
{
    public class Explosion : MonoBehaviour
    {
        [SerializeField] private MeshRenderer meshRenderer;
        [SerializeField] private AnimationCurve progressCurve;
        [SerializeField] private VisualEffect[] particles;
        [SerializeField] private bool beginOnAwake = false;
        [SerializeField] private float duration = 1.0f;

        private int PROGRESS_ID = Shader.PropertyToID("Progress");

        private float currentProgress = 0.0f;
        private bool isExploding = false;
        private Material material;

        private void Start()
        {
            material = meshRenderer.material;
            meshRenderer.enabled = false;

            if (beginOnAwake)
            {
                Begin();
            }
        }

        private void Update()
        {
            if (isExploding)
            {
                float percentage = currentProgress / duration;
                material.SetFloat(PROGRESS_ID, progressCurve.Evaluate(percentage));
                currentProgress += Time.deltaTime;

                if(currentProgress >= duration)
                {
                    isExploding = false;
                    meshRenderer.enabled = false;
                }
            }    
        }

        [ContextMenu("Begin")]
        public void Begin()
        {
            isExploding = true;
            currentProgress = 0.0f;
            meshRenderer.enabled = true;

            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }
        }
    }
}

