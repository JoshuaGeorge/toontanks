using UnityEngine;

public class ProjectileTrajectoryDebug : MonoBehaviour
{
    public Transform transform1;
    public Transform transform2;

    public LineRenderer lineRenderer;

    public int samples = 10;
    [Range(0, 1.0f)]
    public float height = 1.0f;


    // Update is called once per frame
    void Update()
    {
        lineRenderer.positionCount = samples;
        for (int i = 0; i < samples; i++)
        {
            float x = Mathf.Lerp(transform1.position.x, transform2.position.x, (float)i/(float)(samples-1));
            float height = GetHeight(x);
            float z = Mathf.Lerp(transform1.position.z, transform2.position.z, (float)i / (float)(samples - 1));
            lineRenderer.SetPosition(i, new Vector3(x, height, z));  
        }
    }

    private float GetHeight(float x)
    {
        float transform1PosX = transform1.position.x * -1.0f;
        float transform2PosX = transform2.position.x * -1.0f;
        return -height * (Mathf.Pow(x, 2) + (transform1PosX * x) + (transform2PosX * x) + (transform1PosX * transform2PosX));
    }
}
