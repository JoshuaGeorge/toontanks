using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasDebugDisplay : MonoBehaviour
{
    private class DebugText
    {
        public Text Title;
        public Text Value;
    }

    [SerializeField]
    private GameObject debugTextObject;

    [SerializeField]
    private Transform verticalLayoutParent;

    private Dictionary<string, DebugText> debugTextObjects = new Dictionary<string, DebugText>();

    public void SetDebugText(string title, System.Single value)
    {
        SetDebugText(title, value.ToString());
    }

    public void SetDebugText(string title, bool value)
    {
        SetDebugText(title, value.ToString());
    }


    public void SetDebugText(string title, string value)
    {
        if (debugTextObjects.ContainsKey(title))
        {
            debugTextObjects[title].Value.text = value;
        }
        else
        {
            GameObject newDebugTextObj = Instantiate(debugTextObject, verticalLayoutParent);
            DebugText debugText = new DebugText
            {
                Title = newDebugTextObj.transform.GetChild(0).GetComponent<Text>(),
                Value = newDebugTextObj.transform.GetChild(1).GetComponent<Text>()
            };

            debugText.Title.text = title;
            debugText.Value.text = value;
            debugTextObjects.Add(title, debugText);

            newDebugTextObj.name = title;
            newDebugTextObj.SetActive(true);
        }
    }
}
