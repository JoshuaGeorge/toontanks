using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this as T;
        }
        else
        {
            Debug.LogError("There should only be one instance of " + nameof(T) + " at once!");
            Destroy(gameObject);
        }
    }
}
