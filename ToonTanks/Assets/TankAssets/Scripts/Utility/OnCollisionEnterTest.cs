using UnityEngine;
using UnityEngine.Events;

public class OnCollisionEnterTest : MonoBehaviour
{
    public UnityEvent OnCollisionEnterCallback;

    private void OnCollisionEnter(Collision collision)
    {
        OnCollisionEnterCallback.Invoke();
    }
}
