
public interface IDrawDebugDisplay 
{
    void OnDrawDebugDisplay(CanvasDebugDisplay debugDisplay);
}
