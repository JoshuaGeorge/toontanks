using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tanks.Utility
{
    public class GameObjectPool<T> where T : MonoBehaviour
    {
        private GameObject prefab;
        private Vector3 inactiveStatePosition = new Vector3(999.0f, 999.0f, 999.0f);

        private List<Tuple<GameObject, T>> pool;
        private int currentIndex = 0;

        public GameObjectPool(GameObject prefabReference, int poolStartSize = 5)
        {
            Debug.Assert(prefabReference.GetComponent<T>() != null, "The referenced prefab in GameObjectPool must contain a component of type " + nameof(T));

            prefab = prefabReference;
            pool = new List<Tuple<GameObject, T>>(poolStartSize);
            for (int i = 0; i < poolStartSize; ++i)
            {
                CreateAndAddObjectToPool();
            }
        }

        public Tuple<GameObject, T> GetPair(Vector3 position, Quaternion rotation)
        {
            Tuple<GameObject, T> pooledObject = TryGet();
            pooledObject.Item1.transform.position = position;
            pooledObject.Item1.transform.rotation = rotation;
            pooledObject.Item1.SetActive(true);
            OnObjectRetrieved(pooledObject.Item2);
            return pooledObject;
        }

        public GameObject GetObject(Vector3 position, Quaternion rotation)
        {
            Tuple<GameObject, T> pooledObject = TryGet();
            pooledObject.Item1.transform.position = position;
            pooledObject.Item1.transform.rotation = rotation;
            pooledObject.Item1.SetActive(true);
            OnObjectRetrieved(pooledObject.Item2);
            return pooledObject.Item1;
        }

        public GameObject GetObject()
        {
            Tuple<GameObject, T> pooledObject = TryGet();
            OnObjectRetrieved(pooledObject.Item2);
            return pooledObject.Item1;
        }

        public T Get(Vector3 position, Quaternion rotation)
        {
            Tuple<GameObject, T> pooledObject = TryGet();
            pooledObject.Item1.transform.position = position;
            pooledObject.Item1.transform.rotation = rotation;
            pooledObject.Item1.SetActive(true);
            OnObjectRetrieved(pooledObject.Item2);
            return pooledObject.Item2;
        }

        public T Get()
        {
            T component = TryGet().Item2;
            component.gameObject.SetActive(true);
            OnObjectRetrieved(component);
            return component;
        }

        private Tuple<GameObject, T> TryGet()
        {
            int poolSize = pool.Count;
            for (int i = 0; i < poolSize; ++i)
            {
                if (pool[currentIndex].Item1.activeSelf == false)
                {
                    Tuple<GameObject, T> found = pool[currentIndex];
                    currentIndex = (currentIndex + 1) % poolSize;
                    return found;
                }

                currentIndex = (currentIndex + 1) % poolSize;
            }

            // every object in the pool is being used therefore we must expand the pool
            Debug.LogWarning("A GameObjectPool<" + nameof(T) + "> has reached its limit and must therefore expand. Consider increasing the pool's start size.");

            Tuple<GameObject, T> newlyAdded = CreateAndAddObjectToPool();
            currentIndex = pool.Count - 1;
            return newlyAdded;
        }

        private Tuple<GameObject, T> CreateAndAddObjectToPool()
        {
            GameObject spawned = GameObject.Instantiate(prefab, inactiveStatePosition, Quaternion.identity);
            spawned.SetActive(false);
            Tuple<GameObject, T> newPooledObject = new Tuple<GameObject, T>(spawned, spawned.GetComponent<T>());
            pool.Add(newPooledObject);
            OnObjectAdded(newPooledObject.Item2);
            return newPooledObject;
        }

        protected virtual void OnObjectAdded(T component)
        {
        }

        protected virtual void OnObjectRetrieved(T component)
        {
        }
    }

}
