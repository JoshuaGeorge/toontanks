using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tanks.AI;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(AITankController))]
public class AITankControllerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.LabelField("Display Gizmos", EditorStyles.boldLabel);

        AITankController controller = target as AITankController;
        controller.displayCalculateAimPositionsDebug = EditorGUILayout.Toggle("Draw Aim Positions", controller.displayCalculateAimPositionsDebug);
        controller.displayshootActionsDebug = EditorGUILayout.Toggle("Draw Shoot Actions", controller.displayshootActionsDebug);
        controller.displayMovePositionDebug = EditorGUILayout.Toggle("Draw Move Positions", controller.displayMovePositionDebug);
        controller.displayCalculateDirectionDebug = EditorGUILayout.Toggle("Draw Directions", controller.displayCalculateDirectionDebug);
        controller.displayUseEquipmentDebug = EditorGUILayout.Toggle("Draw Use Equipment", controller.displayUseEquipmentDebug);
    }
}

#endif
