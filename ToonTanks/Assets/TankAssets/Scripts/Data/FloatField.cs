using UnityEngine;

[CreateAssetMenu(fileName = "Blank Float Field", menuName = "Data/FloatField", order = 1)]
public class FloatField : DataField<float>
{
}
