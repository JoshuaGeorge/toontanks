using UnityEngine;

[CreateAssetMenu(fileName = "Blank Int Field", menuName = "Data/IntField", order = 1)]
public class IntField : DataField<int>
{
}
