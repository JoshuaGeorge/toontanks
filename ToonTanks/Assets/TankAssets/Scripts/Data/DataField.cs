using UnityEngine;

public class DataField<T> : ScriptableObject
{
    public T value;
}
