using UnityEngine;
using Tanks.VFX;
using Tanks.Interfaces;
using Tanks.Structs;

namespace Tanks.Weapons
{
    public class Mine : MonoBehaviour
    {
        [SerializeField] private Explosion explosion;
        [SerializeField] private float radius = 10.0f;
        [SerializeField] private int damageAmount = 100;

        bool explode = false;
        private Collider[] hit;
        private MeshRenderer meshRenderer;
        private Collider mineCollider;
        private Transform owner;

        private void Awake()
        {
            meshRenderer = GetComponent<MeshRenderer>();
            mineCollider = GetComponent<BoxCollider>();
            hit = new Collider[5];

            mineCollider.enabled = false;
        }

        private void Update()
        {
            if (explode)
            {
                Damage damage;
                damage.Value = damageAmount;
                damage.Damager = gameObject;
                damage.ImpactVelocity = Vector3.zero;
                int count = Physics.OverlapSphereNonAlloc(transform.position, radius, hit);

                for (int i = 0; i < count; i++)
                {
                    TryApplyExplosionDamage(hit[i].gameObject, damage);
                }

                explosion.Begin();
                meshRenderer.enabled = false;
                explode = false;
                mineCollider.enabled = false;
            }
        }

        public void PlaceMine(Vector3 position, Transform owner)
        {
            this.owner = owner;
            transform.parent = null;
            transform.position = position;
            mineCollider.enabled = true;
        }

        private void TryApplyExplosionDamage(GameObject actor, Damage damage)
        {
            IDamageable damagedObject = actor.GetComponent<IDamageable>();
            if (damagedObject != null)
            {
                damagedObject.TakeDamage(damage);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform != owner && other.GetComponent<Rigidbody>())
            {
                explode = true;
            }
        }

        private void OnEnable()
        {
            explode = false;
            meshRenderer.enabled = true;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }

}
