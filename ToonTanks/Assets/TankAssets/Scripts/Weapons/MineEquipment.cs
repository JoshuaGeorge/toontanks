using UnityEngine;
using Tanks.Utility;

namespace Tanks.Weapons
{
    public class MineEquipment : BaseEquipment
    {
        [SerializeField] private int mineCount = 3;
        [SerializeField] private Mine minePrefab;

        private GameObjectPool<Mine> mineObjectPool;
        private int numberOfMinesUsed = 0;

        private void Awake()
        {
            mineObjectPool = new GameObjectPool<Mine>(minePrefab.gameObject, mineCount);
        }

        public override void OnEquipped(Transform owner)
        {
            transform.SetParent(owner);
            transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        }

        public override void UseEquipment()
        {
            if(numberOfMinesUsed < mineCount)
            {
                numberOfMinesUsed++;
                mineObjectPool.Get().PlaceMine(transform.position, transform.parent);
            }
        }
    }
}
