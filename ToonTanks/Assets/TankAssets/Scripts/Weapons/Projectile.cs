using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VFX;
using Tanks.Interfaces;
using Tanks.Structs;

namespace Tanks.Weapons
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private FloatField speed;
        [Tooltip("How many seconds after being activated should the projectile deactivate?")]
        [SerializeField] private float selfDestructTimer = 10.0f;
        [Tooltip("How many times can this projectile bounce off a wall before deactivating?")]
        [Range(0, 5)]
        [SerializeField] private int numberOfAllowedBounces = 2;
        [SerializeField] private UnityEvent OnShoot;

        [Header("Visuals")]
        [SerializeField] private VisualEffect muzzleFlashParticlesAsset;
        [SerializeField] private VisualEffect bounceParticlesAsset;
        [SerializeField] private VisualEffect smokePlumeParticlesAsset;
        [SerializeField] private VisualEffect destroyedParticlesAsset;

        private Rigidbody rigidBody;
        private Vector3 currentMoveDirection = new Vector3(0.0f, 0.0f, 0.0f);

        private VisualEffect muzzleFlashParticlesInstance;
        private VisualEffect bounceParticlesInstance;
        private VisualEffect smokePlumeParticlesInstance;
        private VisualEffect destroyedParticlesInstance;

        private float currentAliveTime = 0.0f;
        private LayerMask wallLayer;
        private int numberOfBounces = 0;

        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            wallLayer = LayerMask.NameToLayer("Wall");

            muzzleFlashParticlesInstance = Instantiate(muzzleFlashParticlesAsset);
            bounceParticlesInstance = Instantiate(bounceParticlesAsset);
            smokePlumeParticlesInstance = Instantiate(smokePlumeParticlesAsset);
            destroyedParticlesInstance = Instantiate(destroyedParticlesAsset);
        }

        private void Update()
        {
            currentAliveTime += Time.deltaTime;

            if (currentAliveTime > selfDestructTimer || numberOfBounces > numberOfAllowedBounces)
            {
                Deactivate();
            }
        }

        private void FixedUpdate()
        {
            if(rigidBody.velocity.sqrMagnitude > 0.1f)
            {
                transform.LookAt(transform.position + rigidBody.velocity, Vector3.up);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            GameObject collidedObject = collision.collider.gameObject;
            // have we collided with a wall?
            if(collidedObject.layer == wallLayer.value)
            {
                numberOfBounces++;
                PlayVFX((numberOfBounces > numberOfAllowedBounces) ? destroyedParticlesInstance : bounceParticlesInstance);
            }
            else
            {
                IDamageable damagedObject = collidedObject.GetComponent<IDamageable>();
                if (damagedObject != null)
                {
                    Damage damage = new Damage();
                    damage.Value = 100;
                    damage.Damager = gameObject;
                    damage.ImpactVelocity = rigidBody.velocity;
                    damagedObject.TakeDamage(damage);
                }

                PlayVFX(destroyedParticlesInstance);
                Deactivate();
            }
        }

        public void Shoot(Vector3 initialDirection)
        {
            OnShoot.Invoke();
            PlayVFX(smokePlumeParticlesInstance);
            PlayVFX(muzzleFlashParticlesInstance);
            numberOfBounces = 0;
            currentAliveTime = 0.0f;
            currentMoveDirection = initialDirection;
            rigidBody.AddForce(currentMoveDirection * speed.value);
        }

        private void PlayVFX(VisualEffect vfx)
        {
            vfx.transform.position = transform.position;
            vfx.Play();
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
            rigidBody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
            rigidBody.Sleep();
        }

        [ContextMenu("Shoot Test")]
        public void ShootTest()
        {
            Shoot(transform.forward);
        }
    }

}
