using Tanks.Interfaces;

namespace Tanks.Weapons
{
    public class EquipmentSlot
    {
        private IEquipment equipment;

        public void SetEquipment(IEquipment equipment)
        {
            this.equipment = equipment;
        }

        public void TryUseEquipment()
        {
            if(equipment != null)
            {
                equipment.UseEquipment();
            }
        }
    }
}
