
using UnityEngine;
using Tanks.Interfaces;


namespace Tanks.Weapons
{
    public abstract class BaseEquipment : MonoBehaviour, IEquipment
    {
        public abstract void OnEquipped(Transform owner);
        public abstract void UseEquipment();
    }
}

