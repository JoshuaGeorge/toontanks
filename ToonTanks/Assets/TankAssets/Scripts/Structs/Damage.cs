using UnityEngine;

namespace Tanks.Structs
{
    public struct Damage
    {
        public int Value;
        public GameObject Damager;
        public Vector3 ImpactVelocity;
    }
}
