using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Tanks.UI
{
    [System.Serializable]
    public struct LevelDescriptor
    {
        public string displayName;
        public string sceneName;
        public Texture2D thumbnail;
    }

    public class LevelSelector : MonoBehaviour
    {
        [SerializeField] private GameObject levelSelectPrefab;
        [SerializeField] private Transform gridParent;
        [SerializeField] private LevelDescriptor[] levels;

        void Awake()
        {
            for (int i = 0; i < levels.Length; i++)
            {
                GameObject levelSelector = Instantiate(levelSelectPrefab, gridParent);
                levelSelector.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Sprite.Create(levels[i].thumbnail, new Rect(0.0f, 0.0f, levels[i].thumbnail.width, levels[i].thumbnail.height), new Vector2(0.5f, 0.5f), 100.0f);
                levelSelector.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = levels[i].displayName;
                int levelIndex = i;
                levelSelector.GetComponentInChildren<Button>().onClick.AddListener(() => { Game.GameManager.Instance.LoadLevel(levels[levelIndex].sceneName, levelIndex+1); });
            }
        }
    }
}
