using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tanks.Tank;

public class TankRicochetTest : MonoBehaviour
{
    public GameObject playerTankPrefab;
    public GameObject enemyTankPrefab;

    private TankController playerController;
    private TankController aiController;
    public float reflectionLength = 10.0f;

    private Vector3 reflectedAimDir;
    private Vector3 hitPoint;
    private Vector3 virtualAIPosition;

    // Start is called before the first frame update
    void Start()
    {
        playerController = Instantiate(playerTankPrefab).GetComponent<TankController>();
        aiController = Instantiate(enemyTankPrefab).GetComponent<TankController>();
        aiController.AssignLevelDetails(null);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerAimDirection = playerController.GetTurretAimDirection();

        Ray ray = new Ray(playerController.GetPosition(), playerAimDirection);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        if (hit.collider != null)
        {
            reflectedAimDir = Vector3.Reflect(playerAimDirection, hit.normal);
            hitPoint = hit.point;

            Vector3 tangent = Vector3.Cross(hit.normal, Vector3.up);
            Vector3 virtualAiPos = Vector3.Reflect(hit.point - aiController.GetPosition(), tangent);
            virtualAIPosition = virtualAiPos;
        }
    }

    private void OnDrawGizmos()
    {
        if(playerController != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(playerController.GetPosition(), hitPoint);
            Gizmos.DrawLine(hitPoint, hitPoint + (reflectedAimDir * reflectionLength));

            Gizmos.color = new Color(0.8f, 0.2f, 0.1f);
            Gizmos.DrawLine(playerController.GetPosition(), aiController.GetPosition());

            //Vector3 playerToAI = aiController.GetPosition() - playerController.GetPosition();
            //Vector3 playerToAimHitPoint = hitPoint - playerController.GetPosition();
            //float dot = Vector3.Dot(playerToAI.normalized, playerToAimHitPoint.normalized);
            //Gizmos.DrawSphere(playerController.GetPosition() + Vector3.Project(playerToAimHitPoint, playerToAI), 0.5f);

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(hitPoint + virtualAIPosition, 1.0f);
        }
    }
}
