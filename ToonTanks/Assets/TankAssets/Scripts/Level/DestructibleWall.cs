using UnityEngine;
using Tanks.Interfaces;
using Tanks.Structs;
using UnityEngine.VFX;

namespace Tanks.Level
{
    public class DestructibleWall : MonoBehaviour, IDamageable
    {
        [SerializeField] private MeshRenderer mesh;
        [SerializeField] private VisualEffect vfx;


        public void TakeDamage(Damage damage)
        {
            mesh.enabled = false;
            vfx.Play();
        }
    }
}


