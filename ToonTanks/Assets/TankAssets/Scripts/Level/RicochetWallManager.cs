using UnityEngine;

public class RicochetWallManager
{
    private Wall[] walls;
    private int wallLayerMask;
    private int playerObstructionsLayerMask;

    public RicochetWallManager(Wall[] walls)
    {
        this.walls = walls;
        wallLayerMask = LayerMask.GetMask("Wall");
        playerObstructionsLayerMask = LayerMask.GetMask("Wall", "Players");
    }

    public Vector2 GetRicochetDirection(Vector3 ricochetUserPosition, Transform targetTransform, out Wall selectedWall)
    {
        Vector2 userPosition2D = new Vector2(ricochetUserPosition.x, ricochetUserPosition.z);
        Vector2 targetPosition2D = new Vector2(targetTransform.position.x, targetTransform.position.z);
        for (int i = 0; i < walls.Length; i++)
        {
            Wall wall = walls[i];
            
            if (wall.IsVisibleFromPositiveX(userPosition2D) && wall.IsVisibleFromPositiveX(targetPosition2D))
            {
                selectedWall = wall;
                Vector2 reflectedTargetPos = wall.GetReflectedPositionFromPositiveX(targetPosition2D);
                Vector2 ricochetDirection2D = reflectedTargetPos - userPosition2D;
                if (DoesRicochetHitTarget(ricochetUserPosition, new Vector3(ricochetDirection2D.x, ricochetUserPosition.y, ricochetUserPosition.y), targetTransform))
                {
                    return reflectedTargetPos;
                }
            }
            
            if (wall.IsVisibleFromPositiveZ(userPosition2D) && wall.IsVisibleFromPositiveZ(targetPosition2D))
            {
                selectedWall = wall;
                Vector2 reflectedTargetPos = wall.GetReflectedPositionFromPositiveZ(targetPosition2D);
                Vector2 ricochetDirection2D = reflectedTargetPos - userPosition2D;
                if (DoesRicochetHitTarget(ricochetUserPosition, new Vector3(ricochetDirection2D.x, ricochetUserPosition.y, ricochetUserPosition.y), targetTransform))
                {
                    return reflectedTargetPos;
                }
            }
            
            if (wall.IsVisibleFromNegativeX(userPosition2D) && wall.IsVisibleFromNegativeX(targetPosition2D))
            {
                selectedWall = wall;
                Vector2 reflectedTargetPos = wall.GetReflectedPositionFromNegativeX(targetPosition2D);
                Vector2 ricochetDirection2D = reflectedTargetPos - userPosition2D;
                if (DoesRicochetHitTarget(ricochetUserPosition, new Vector3(ricochetDirection2D.x, ricochetUserPosition.y, ricochetDirection2D.y), targetTransform))
                {
                    return reflectedTargetPos;
                }
            }
            
            if (wall.IsVisibleFromNegativeZ(userPosition2D) && wall.IsVisibleFromNegativeZ(targetPosition2D))
            {
                selectedWall = wall;
                Vector2 reflectedTargetPos = wall.GetReflectedPositionFromNegativeZ(targetPosition2D);
                Vector2 ricochetDirection2D = reflectedTargetPos - userPosition2D;
                if (DoesRicochetHitTarget(ricochetUserPosition, new Vector3(ricochetDirection2D.x, ricochetUserPosition.y, ricochetDirection2D.y), targetTransform))
                {
                    return reflectedTargetPos;
                }
            }
        }

        selectedWall = null;
        return Vector2.zero;
    }

    private bool DoesRicochetHitTarget(Vector3 origin, Vector3 direction, Transform target)
    {
        Ray rayToWall = new Ray(origin, direction);
        RaycastHit hit;
        if (Physics.Raycast(rayToWall, out hit, direction.magnitude, wallLayerMask))
        {
            Ray reflectedRayOffWall = new Ray(hit.point, Vector3.Reflect(direction, hit.normal));
            if (Physics.Raycast(reflectedRayOffWall, out hit, 50.0f, playerObstructionsLayerMask))
            {
                return hit.transform.Equals(target);
            }
        }

        return false;
    }
}
