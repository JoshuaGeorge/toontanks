using UnityEngine;

namespace Tanks.Level
{
    [CreateAssetMenu(fileName = "New Tank Configuration", menuName = "Tanks/TankConfiguration", order = 1)]
    public class TankConfiguration : ScriptableObject
    {
        public GameObject tankPrefab;
        public GameObject projectilePrefab;

        [Header("Attributes")]
        public FloatField movementSpeed;
        public FloatField fireRateCooldown;
        [Range(0, 5)]
        public int projectileLimit;
        [Range(0, 20)]
        public int mineLimit;

        public void SetupTankController(Tank.TankController controller)
        {
            controller.SetProjectile(projectilePrefab);
            controller.SetMaximumAliveProjectilesLimit(projectileLimit);
            controller.SetMovementSpeed(movementSpeed.value);
            controller.SetProjectileFireRate(fireRateCooldown.value);
        }
    }
}

