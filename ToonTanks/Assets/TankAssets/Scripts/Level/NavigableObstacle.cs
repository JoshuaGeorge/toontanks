using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NavigableObstacle : MonoBehaviour
{
    [Flags]
    public enum ObstacleType
    {
        Stationary = 1,
        Projectile = 2
    }

    [SerializeField]
    private ObstacleType obstacleType;

    [SerializeField]
    private float avoidRange = 2.0f;

    private Rigidbody rigidBody;

    private void Awake()
    {
        if(obstacleType == ObstacleType.Projectile)
        {
            rigidBody = GetComponent<Rigidbody>();
        }
    }

    public ObstacleType Type => obstacleType;
    public Vector2 Position2D => new Vector2(transform.position.x, transform.position.z);
    public Vector3 Position => transform.position;
    public Vector3 Velocity => rigidBody != null ? rigidBody.velocity : new Vector3(0.0f, 0.0f, 0.0f);
    public Vector2 Velocity2D => rigidBody != null ? new Vector2(rigidBody.velocity.x, rigidBody.velocity.z) : new Vector2(0.0f, 0.0f);
    public float AvoidanceRange => avoidRange;

}
