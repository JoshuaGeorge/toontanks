using UnityEngine;

namespace Tanks.Level
{
    public class TankSpawnPosition : MonoBehaviour
    {
        [SerializeField] private float validAreaRadius = 2.0f;

        private Vector3 spawnPosition;
        private LayerMask groundLayerMask;

        private void Awake()
        {
            groundLayerMask = LayerMask.GetMask("Ground");

            // do a ray cast down to find a floor position.
            RaycastHit hit;
            bool result = Physics.Raycast(transform.position, Vector3.down, out hit, 999.0f, groundLayerMask);
            Debug.Assert(result == true, "A TankSpawnPosition failed to find a valid ground point! Make sure the spawn position is above a plane with the 'Ground' layer! Using a default of 0,0,0 as the spawn point.");

            if (result)
            {
                spawnPosition = hit.point;
            }
            else
            {
                Debug.LogWarning(name + " (TankSpawnPosition) was unable to find a spawn position on a plane with the 'Ground' layer assigned");
            }
        }

        public bool IsValid()
        {
            // do a sphere collider check to make sure the surrounding area is clear and a tank can be spawned here.
            Collider[] colliders = Physics.OverlapSphere(spawnPosition, validAreaRadius, ~groundLayerMask);
            return colliders.Length == 0;
        }

        public Vector3 GetPosition()
        {
            return spawnPosition;
        }
        
        public Quaternion GetRotation()
        {
            return Quaternion.LookRotation(transform.forward);
        }

        private void OnDrawGizmos()
        {
            // Draw a semitransparent blue cube at the transforms position
            Gizmos.DrawIcon(transform.position + new Vector3(0.0f, 1.0f, 0.0f), "TankSpawnIcon.png", true);
        }
    }
}


