using System.Collections.Generic;
using UnityEngine;
using Tanks.Weapons;

namespace Tanks.Level
{
    [System.Serializable]
    public struct TankSpawnData
    {
        public TankConfiguration Configuration;
        public TankSpawnPosition Position;
        public BaseEquipment StartingEquipment;
        public bool Respawnable;
    }

    public class TankSpawner
    {
        private TankSpawnData[] spawnData;
        private List<GameObject> spawnedTanks;
        private readonly Vector3 TANK_POSITION_OFFSET = new Vector3(0.0f, 0.5f, 0.0f);

        public TankSpawner(TankSpawnData[] spawnData)
        {
            this.spawnData = spawnData;
            spawnedTanks = new List<GameObject>(spawnData.Length);

            for (int i = 0; i < spawnData.Length; i++)
            {
                spawnedTanks.Add(Create(spawnData[i]));
            }
        }

        public GameObject Spawn(int index)
        {
            GameObject tank = spawnedTanks[index];
            TankSpawnData data = spawnData[index];

            tank.transform.position = data.Position.GetPosition();
            tank.transform.rotation = data.Position.GetRotation();

            tank.GetComponent<Tank.Health>().Respawn(true);
            tank.SetActive(true);

            return tank;
        }

        public void SpawnAll(ref List<GameObject> tankList)
        {
            for (int i = 0; i < spawnedTanks.Count; i++)
            {
                tankList.Add(Spawn(i));
            }
        }

        public int NumberOfTanks()
        {
            return spawnData.Length;
        }

        private GameObject Create(TankSpawnData data)
        {
            if (data.Position.IsValid())
            {
                GameObject spawnedTank = GameObject.Instantiate(data.Configuration.tankPrefab, data.Position.GetPosition() + TANK_POSITION_OFFSET, data.Position.GetRotation());
                Tank.TankController controller = spawnedTank.GetComponent<Tank.TankController>();

                if (controller)
                {
                    data.Configuration.SetupTankController(controller);
                }

                return spawnedTank;
            }
            else
            {
                Debug.LogError("A Tank could not be spawned as its SpawnPosition was not valid!");
                return null;
            }
        }
    }

}

