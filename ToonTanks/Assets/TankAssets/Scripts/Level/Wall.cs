using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Wall : MonoBehaviour
{
    private struct Side
    {
        public Vector2 Position;
        public Vector2 Normal;
        public Vector2 Tangent;
        public bool IsVisible;

        public Side(Vector2 position, Vector2 normal, Vector2 tangent, bool isVisible)
        {
            Position = position;
            Normal = normal;
            Tangent = tangent;
            IsVisible = isVisible;
        }
    }

    [SerializeField]
    bool positiveXVisible = true;
    [SerializeField]
    bool negativeXVisible = true;
    [SerializeField]
    bool positiveZVisible = true;
    [SerializeField]
    bool negativeZVisible = true;

    private Side positiveXSide;
    private Side negativeXSide;
    private Side positiveZSide;
    private Side negativeZSide;

    private BoxCollider wallCollider;

    private void Awake()
    {
        wallCollider = GetComponent<BoxCollider>();

        Vector3 wallExtents = wallCollider.bounds.extents;
        positiveXSide = new Side(GetPosition2D() + Vector2.right * wallExtents.x, Vector2.right, Vector2.up, positiveXVisible);
        negativeXSide = new Side(GetPosition2D() + Vector2.left * wallExtents.x, Vector2.left, Vector2.down, negativeXVisible);
        positiveZSide = new Side(GetPosition2D() + Vector2.up * wallExtents.z, Vector2.up, Vector2.right, positiveZVisible);
        negativeZSide = new Side(GetPosition2D() + Vector2.down * wallExtents.z, Vector2.down, Vector2.left, negativeZVisible);
    }

    public bool IsVisibleFromPositiveX(Vector2 fromPosition)
    {
        return IsSideVisible(positiveXSide, fromPosition);
    }

    public bool IsVisibleFromNegativeX(Vector2 fromPosition)
    {
        return IsSideVisible(negativeXSide, fromPosition);
    }

    public bool IsVisibleFromPositiveZ(Vector2 fromPosition)
    {
        return IsSideVisible(positiveZSide, fromPosition);
    }

    public bool IsVisibleFromNegativeZ(Vector2 fromPosition)
    {
        return IsSideVisible(negativeZSide, fromPosition);
    }

    public Vector2 GetReflectedPositionFromPositiveX(Vector2 inPosition)
    {
        return GetReflectedPositionFromSide(positiveXSide, inPosition);
    }

    public Vector2 GetReflectedPositionFromNegativeX(Vector2 inPosition)
    {
        return GetReflectedPositionFromSide(negativeXSide, inPosition);
    }

    public Vector2 GetReflectedPositionFromPositiveZ(Vector2 inPosition)
    {
        return GetReflectedPositionFromSide(positiveZSide, inPosition);
    }

    public Vector2 GetReflectedPositionFromNegativeZ(Vector2 inPosition)
    {
        return GetReflectedPositionFromSide(negativeZSide, inPosition);
    }

    private Vector2 GetPosition2D()
    {
        return new Vector2(transform.position.x, transform.position.z);
    }

    private bool IsSideVisible(Side side, Vector2 fromPosition)
    {
        Vector2 toPosition = fromPosition - side.Position;
        return side.IsVisible && Vector2.Dot(toPosition, side.Normal) > 0;
    }

    private Vector2 GetReflectedPositionFromSide(Side side, Vector2 inPosition)
    {
        return side.Position + Vector2.Reflect(side.Position - inPosition, side.Tangent);
    }

    //private void OnDrawGizmos()
    //{
    //    if (targetTransform == null || userTransform == null) return;

    //    Vector2 testPos2D = new Vector2(targetTransform.position.x, targetTransform.position.z);

    //    Gizmos.color = IsVisibleFromPositiveX(testPos2D) ? Color.red : Color.grey;
    //    Gizmos.DrawLine(new Vector3(positiveXSide.Position.x, 0.0f, positiveXSide.Position.y), new Vector3(positiveXSide.Position.x, 0.0f, positiveXSide.Position.y) + new Vector3(positiveXSide.Normal.x, 0.0f, positiveXSide.Normal.y));
    //    Gizmos.color = IsVisibleFromNegativeX(testPos2D) ? Color.red : Color.grey;
    //    Gizmos.DrawLine(new Vector3(negativeXSide.Position.x, 0.0f, negativeXSide.Position.y), new Vector3(negativeXSide.Position.x, 0.0f, negativeXSide.Position.y) + new Vector3(negativeXSide.Normal.x, 0.0f, negativeXSide.Normal.y));
    //    Gizmos.color = IsVisibleFromPositiveZ(testPos2D) ? Color.red : Color.grey;
    //    Gizmos.DrawLine(new Vector3(positiveZSide.Position.x, 0.0f, positiveZSide.Position.y), new Vector3(positiveZSide.Position.x, 0.0f, positiveZSide.Position.y) + new Vector3(positiveZSide.Normal.x, 0.0f, positiveZSide.Normal.y));
    //    Gizmos.color = IsVisibleFromNegativeZ(testPos2D) ? Color.red : Color.grey;
    //    Gizmos.DrawLine(new Vector3(negativeZSide.Position.x, 0.0f, negativeZSide.Position.y), new Vector3(negativeZSide.Position.x, 0.0f, negativeZSide.Position.y) + new Vector3(negativeZSide.Normal.x, 0.0f, negativeZSide.Normal.y));

    //    if (IsVisibleFromPositiveZ(testPos2D))
    //    {
    //        Vector2 reflectedPos = GetReflectedPositionFromPositiveZ(testPos2D);
    //        Gizmos.color = Color.cyan;
    //        Gizmos.DrawWireCube(new Vector3(reflectedPos.x, targetTransform.position.y, reflectedPos.y), Vector3.one);
    //    }

    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(userTransform.position, new Vector3(ricochetPoint.x, userTransform.position.y, ricochetPoint.y));
    //    Gizmos.color = Color.cyan;
    //    Gizmos.DrawLine(new Vector3(ricochetPoint.x, userTransform.position.y, ricochetPoint.y), targetTransform.position);
    //}
}
