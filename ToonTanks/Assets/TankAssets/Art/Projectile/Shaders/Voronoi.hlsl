
float3 N33(float3 p) {
	float3 a = frac(p.xyz * float3(123.34, 234.34, 345.65));
	a += dot(a, a + 34.45);
	return frac(float3(a.x * a.y, a.y * a.z, a.z * a.x));
}


void Voronoi3D_float(float3 position, float3 tiling, out float output) 
{
	position = position * tiling;

	float3 positionId = floor(position);
	float minDistance = 2.0f;

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			for (int z = -1; z <= 1; z++) 
			{
				float3 cellId = positionId + float3(x, y, z);

				// returns a random 3d position between 0 and 1.
				float3 randomPos = N33(cellId);
				// get a random position within our grid cell
				randomPos = cellId + randomPos;

				float dist = distance(position, randomPos);
				// get the distance between our position and the random position in the grid cell
				minDistance = min(minDistance, dist);
			}
		}
	}

	output = minDistance;
}