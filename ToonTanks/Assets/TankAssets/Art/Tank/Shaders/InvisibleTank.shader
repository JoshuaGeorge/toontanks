Shader "Unlit/InvisibleTank"
{
    Properties
    {
        _NoiseTex("Noise Texture", 2D) = "" {}
        _DistortStrength("Distortion Strength", Range(0, 1)) = 0.1
        _DistortionSpeed("Distortion Speed", Range(0, 10)) = 1
        _RimSize("Rim Size", Float) = 0.1
        _RimGradient("Rim Gradient", Range(0, 1)) = 0.1
        _RimColor("Rim Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                float3 normal : NORMAL;
                float3 worldSpaceViewDir : TEXCOORD2;
            };

            sampler2D _NoiseTex;
            float _DistortStrength;
            float _DistortionSpeed;
            float _RimSize;
            float _RimGradient;
            float4 _RimColor;

            sampler2D _ColorBuffer;
            float4 _ColorBuffer_ST;
            float4 _ColorBuffer_TexelSize;

            v2f vert (appdata v)
            {
                v2f o;
                float3 worldSpaceVertex = mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.screenPos = ComputeScreenPos(o.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.worldSpaceViewDir =  _WorldSpaceCameraPos - worldSpaceVertex;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 noise = tex2D(_NoiseTex, i.normal.xy + float2(cos(_Time.y * _DistortionSpeed), sin(_Time.y * _DistortionSpeed))).rgb * _DistortStrength;

                float rim = saturate(dot(normalize(i.worldSpaceViewDir), i.normal));
                rim = 1 - smoothstep(_RimSize, _RimSize + _RimGradient, rim);
                float3 rimColor = _RimColor * max(rim, noise.x);

                float2 uvOffset = i.normal.xy * _ColorBuffer_TexelSize.xy;
                float2 uv = (i.screenPos.xy + (uvOffset + noise.xy)) / i.screenPos.w;
                // sample the texture
                fixed4 col = tex2D(_ColorBuffer, uv);
                //fixed4 col = 0;
                return col + fixed4(rimColor, 1.0);
            }
            ENDCG
        }
    }
}
